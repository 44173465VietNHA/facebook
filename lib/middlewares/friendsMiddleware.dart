import 'dart:convert';
import 'dart:developer';

import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction<AppState> getFriendsThunk(String id) {
  return (Store<AppState> store) async {
    log("getFriendsThunk");
    String friends = (await getFriends(id)).body;
    log(friends);
    List<dynamic> list = jsonDecode(friends)['data']['friends'];
    List<UserInfoState> _list = (list).map((i) => UserInfoState.fromJson(i)).toList();
    store.dispatch(SetFriendsAction(friends: _list));
  };
}