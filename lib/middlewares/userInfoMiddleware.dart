import 'dart:convert';
import 'dart:developer';

import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction<AppState> getUserInfoThunk(String id) {
  return (Store<AppState> store) async {
    log("getUserInfoThunk");
    String userInfo = (await getUserInfo(id)).body;
    log(userInfo);
    UserInfoState u = UserInfoState.fromJson(jsonDecode(userInfo)['data']);
    store.dispatch(SetUserInfoAction(userInfo: u));
  };
}