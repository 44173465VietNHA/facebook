import 'dart:convert';

import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<GetFriendResponse> get_user_friends(String authorization, String token,
    int user_id, int index, int count) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/get_user_friends',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': authorization,
    },
    body: jsonEncode(<String, dynamic>{
      'token': token,
      'user_id': user_id,
      'index': index,
      'count': count
    }),
  );
  print(GetFriendResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return GetFriendResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}
