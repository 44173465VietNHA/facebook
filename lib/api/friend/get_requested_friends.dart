import 'dart:convert';

import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;


Future<GetRequestFriendResponse> get_requested_friends(String authorization, String token, int user_id) async {
    var client = new http.Client();
    http.Response rs = await client.post(
      Constants.Root + 'api/get_requested_friends',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            authorization,
      },
      body: jsonEncode(<String, dynamic>{
        'token': token,
        'user_id': user_id,
      }),
    );
    print(GetRequestFriendResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
    return GetRequestFriendResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}