import 'dart:convert';
import 'package:facebook/api/payload/ResponseCode.dart';
import 'package:facebook/api/payload/SetRequestFriendResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<SetRequestFriendResponse> set_request_friend(String authorization, String token, int user_id) async {
    var client = new http.Client();
    http.Response rs = await client.post(
      Constants.Root + 'api/set_request_friend',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            authorization,
      },
      body: jsonEncode(<String, dynamic>{
        'token': token,
        'user_id': user_id,
      }),
    );
    print(SetRequestFriendResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
    return SetRequestFriendResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}