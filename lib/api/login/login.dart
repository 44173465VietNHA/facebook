import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<LoginResponse> login(String phonenumber, String password) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/login',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{'phonenumber': phonenumber, 'password': password}),
  );
  print(LoginResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return LoginResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}

class LoginResponse {
  int code;
  String message;
  LoginData data;
  
    LoginResponse({this.code, this.message, this.data});
  
    factory LoginResponse.fromJson(Map<String, dynamic> json) {
      if (json['data'] != null) {
        return LoginResponse(
          code: json['code'] as int,
          message: json['message'] as String,
          data: LoginData.fromJson(json['data']),
        );
    } else return LoginResponse(
      code: json['code'] as int,
          message: json['message'] as String
    );
    }
  
    @override
    String toString() {
      return '{ ${this.code}, ${this.message} , ${this.data} }';
    }
  }


  class LoginData {
  final int id;
	final String username;
	final String token;
	final String avatar;
	final String active;

  // ignore: non_constant_identifier_names
  LoginData({this.id, this.username, this.avatar, this.token, this.active});

  factory LoginData.fromJson(Map<String, dynamic> json) {
    return LoginData(
        id: json['id'] as int,
        username: json['username'] as String,
        avatar: json['avatar'] as String,
        active: json['active'] as String,
        token: json['token'] as String);
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.username}, ${this.avatar}, ${this.token}, ${this.active} }';
  }
}