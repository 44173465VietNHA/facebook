import 'dart:convert';

import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;


Future<SearchResponse> get_list_post(String authorization, String token,
    int user_id, int index, int count, int last_id) async {
    var client = new http.Client();
    http.Response rs = await client.post(
      Constants.Root + 'api/get_list_posts',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            authorization,
      },


  //     private String token;
	// private Long user_id;
	// private String in_campaign; // 0: lấy tất cả sp, 
	// 		//1: chỉ lấy sp trong chiến dịch khuyến mãi
	// private String latitude; // kinh độ
	// private String longitude; // vi độ
	// private Long last_id; 		// id bài viết trả về lần trước
	// private Integer index;
	// private Integer count;


      body: jsonEncode(<String, dynamic>{
        'token': token,
        'user_id': user_id,
        'in_campaign': 0,
        'latitude': 0,
        'longitude':0,
        'last_id': last_id,
        'index': index,
        'count': count
      }),
    );
    print(SearchResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
    return SearchResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}