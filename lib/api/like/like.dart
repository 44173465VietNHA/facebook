import 'dart:convert';

import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<LikeResponse> like(String authorization, String token, int id) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/like',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': authorization,
    },
    body: jsonEncode(<String, dynamic>{'token': token, 'id': id}),
  );
  print(LikeResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return LikeResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}

class LikeResponse {
  int code;
  String message;
  LikeData data;

  LikeResponse({this.code, this.message, this.data});

  factory LikeResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {

      return LikeResponse(
          code: json['code'] as int,
          message: json['message'] as String,
          data: LikeData.fromJson(json['data']));
    } else {
      return LikeResponse(
          code: json['code'] as int, message: json['message'] as String);
    }
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}

class LikeData {
  final int like;

  // ignore: non_constant_identifier_names
  LikeData({this.like});

  factory LikeData.fromJson(Map<String, dynamic> json) {
    return LikeData(
        like: json['id'] as int
        );
  }

  @override
  String toString() {
    return '{ ${this.like}}';
  }
}
