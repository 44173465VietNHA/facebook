import 'package:facebook/api/payload/GetUserInfoData.dart';

class GetUserInfoResponse {
  int code;
  String message;
  GetUserInfoData data;

  GetUserInfoResponse({this.code, this.message, this.data});

  factory GetUserInfoResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      return GetUserInfoResponse(
        code: json['code'] as int,
        message: json['message'] as String,
        data: GetUserInfoData.fromJson(json['data']),
      );
  } else return GetUserInfoResponse(
    code: json['code'] as int,
        message: json['message'] as String
  );
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.data} }';
  }
}