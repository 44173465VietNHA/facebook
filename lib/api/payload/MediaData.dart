class MediaData {
  final int id;
  final String fileName;
  final String fileDownloadUri;
  final String fileType;
  final int size;

  MediaData({this.id, this.fileName, this.fileDownloadUri, this.fileType, this.size});

  factory MediaData.fromJson(Map<String, dynamic> json) {
    return MediaData(
        id: json['id'] as int,
        fileName: json['fileName'] as String,
        fileDownloadUri: json['fileDownloadUri'] as String,
        fileType: json['fileType'] as String,
        size: json['size'] as int);
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.fileName}, ${this.fileDownloadUri}, ${this.fileType}, ${this.size} }';
  }
}
