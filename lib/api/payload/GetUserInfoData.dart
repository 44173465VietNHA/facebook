
class GetUserInfoData {
  final int id;
	final String username;
	final String created;
	final String avatar;
	final String cover_image;
	final String link;
	final String address;
	final String country;
	final String listing; // so luong ban be
	final String is_friend;

  // ignore: non_constant_identifier_names
  GetUserInfoData({this.id, this.username, this.created, this.avatar, this.cover_image, this.link, this.address, this.country, this.listing, this.is_friend});

  factory GetUserInfoData.fromJson(Map<String, dynamic> json) {
    return GetUserInfoData(
        id: json['id'] as int,
        username: json['username'] as String,
        created: json['created'] as String,
        avatar: json['avatar'] as String,
        cover_image: json['cover_image'] as String,
        link: json['link'] as String,
        address: json['address'] as String,
        country: json['country'] as String,
        listing: json['listing'] as String,
        is_friend: json['is_friend'] as String,
    );
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.username}, ${this.created}, ${this.avatar}, ${this.cover_image} , ${this.link}, ${this.address}, ${this.country}, ${this.listing}, ${this.is_friend}}';
  }
}
