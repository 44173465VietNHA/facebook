import 'package:facebook/api/payload/FriendsData.dart';

class GetFriendData {
  final List<FriendsData> friends;
  final int total;

  GetFriendData({this.friends, this.total});

  factory GetFriendData.fromJson(Map<String, dynamic> json) {
    var tagObjsJson = json['friends'] as List;
    List<FriendsData> _friends =
        tagObjsJson.map((tagJson) => FriendsData.fromJson(tagJson)).toList();
    return GetFriendData(
      friends: _friends,
      total: json['total'] as int,
    );
  }

  @override
  String toString() {
    return '{ ${this.friends}, ${this.total} }';
  }
}
