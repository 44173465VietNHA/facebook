import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;

Future<ChangeInfoResponseToken> first_signup(String phonenumber, String password,
    String first_name, String last_name, String birth_day, String male, String uuid) async {
  var client = new http.Client();
  http.Response rs = await client.post(
    Constants.Root + 'api/first_signup',

    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{
      'phonenumber': phonenumber,
      'password': password,
      'uuid': uuid,
      'first_name': first_name,
      'last_name': last_name,
      'birth_day': birth_day,
      "male": male,
    }),
  );
  print(
      ChangeInfoResponseToken.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
  return ChangeInfoResponseToken.fromJson(
      jsonDecode(utf8.decode(rs.bodyBytes)));
}

class ChangeInfoResponseToken {
  int code;
  String message;
  String token;
  InfoData data;

  ChangeInfoResponseToken({this.code, this.message, this.token,this.data});

  factory ChangeInfoResponseToken.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      return ChangeInfoResponseToken(
        code: json['code'] as int,
        message: json['message'] as String,
        token: json['token'] as String,
        data: InfoData.fromJson(json['data']),
      );
    } else
      return ChangeInfoResponseToken(
          code: json['code'] as int, message: json['message'] as String);
  }

  @override
  String toString() {
    return '{ ${this.code}, ${this.message} , ${this.token} , ${this.data} }';
  }
}

class InfoData {
  final int id;
  final String username;
  final String phonenumber;
  final String created;
  final String avatar;

  // ignore: non_constant_identifier_names
  InfoData({this.id, this.username, this.avatar, this.created, this.phonenumber});

  factory InfoData.fromJson(Map<String, dynamic> json) {
    return InfoData(
        id: json['id'] as int,
        username: json['username'] as String,
        avatar: json['avatar'] as String,
        phonenumber: json['phonenumber'] as String,
        created: json['created'] as String);
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.username}, ${this.avatar}, ${this.phonenumber}, ${this.created} }';
  }
}
