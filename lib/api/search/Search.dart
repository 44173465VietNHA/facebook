import 'dart:convert';

import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:http/http.dart' as http;
import 'package:facebook/common/constants.dart' as Constants;


Future<SearchResponse> search(String authorization, String token, String keyword,
    int user_id, int index, int count) async {
    var client = new http.Client();
    http.Response rs = await client.post(
      Constants.Root + 'api/search',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            authorization,
      },
      body: jsonEncode(<String, dynamic>{
        'token': token,
        'keyword': keyword,
        'user_id': user_id,
        'index': index,
        'count': count
      }),
    );
    print(SearchResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes))));
    return SearchResponse.fromJson(jsonDecode(utf8.decode(rs.bodyBytes)));
}