import 'package:meta/meta.dart';

class NotificationSlotState {
  final String type;
  final String entityId;
  final String avatarUrl;
  final String name;

  NotificationSlotState({@required this.type, @required this.entityId, @required this.avatarUrl, @required this.name});
  factory NotificationSlotState.initial() {
    return NotificationSlotState(type: null, entityId: null, avatarUrl: null, name: null);
  }

  NotificationSlotState copyWith({
    String type,
    String entityId,
    String avatarUrl,
    String name
  }) {
    return NotificationSlotState(type: type ?? this.type, entityId: entityId ?? this.entityId, avatarUrl: avatarUrl ?? this.avatarUrl, name : name ?? this.name);
  }

  NotificationSlotState.fromJson(Map<String, dynamic> json) : type = json['type'], entityId = json['entityId'], avatarUrl= json['avatar'], name = json['name'];
}