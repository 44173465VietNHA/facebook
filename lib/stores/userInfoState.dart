import 'package:meta/meta.dart';

class UserInfoState {
  final String id;
  final String avatar;
  final String username;

  UserInfoState({@required this.id, @required this.avatar, @required this.username});
  factory UserInfoState.initial() {
    return UserInfoState(id: null, avatar: null, username: null);
  }

  UserInfoState copyWith({
    String id,
    String avatar,
    String username
  }) {
    return UserInfoState(id: id ?? this.id, avatar: avatar ?? this.avatar, username : username ?? this.username);
    // return UserInfoState(avatar: "https://i.pinimg.com/originals/2b/d9/c8/2bd9c85e5fd8f5a0bfd3c307a6f8fa54.jpg", username : username ?? this.username);
  }

  UserInfoState.fromJson(Map<String, dynamic> json) : id= json['id'].toString(), avatar= json['avatar'], username = json['username'];

 @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserInfoState && avatar == other.avatar && username == other.username;
}