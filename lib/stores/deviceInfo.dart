class DeviceInfo {
  final String uuid;

  DeviceInfo({this.uuid});

  factory DeviceInfo.initial() {
    return DeviceInfo(uuid: "");
  }

  DeviceInfo copyWith({
    String uuid,
  }) {
    return DeviceInfo(uuid: uuid ?? this.uuid);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is DeviceInfo && uuid == other.uuid;

  @override
  int get hashCode => uuid.hashCode;
}
