import 'package:facebook/stores/notificationState.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'deviceInfo.dart';
import 'exampleState.dart';
import 'toDoState.dart';

class AppState {
  final ExampleProduct exampleProduct;
  final UserUIState userUIState;
  final DeviceInfo deviceInfo;
  final List<Album> listAlbums;
  final List<NotificationSlotState> listNotifications;
  final UserInfoState userInfo;
  final List<UserInfoState> friends;

  AppState({this.exampleProduct, this.userUIState, this.deviceInfo, this.listAlbums, this.listNotifications, this.userInfo, this.friends});

  factory AppState.initial() {
    return AppState(
      exampleProduct: ExampleProduct.initial(),
      userUIState: UserUIState.initial(),
      deviceInfo: DeviceInfo.initial(),
      listAlbums: List<Album>(),
      listNotifications: List<NotificationSlotState>(),
      userInfo: UserInfoState.initial(),
      friends: List<UserInfoState>()
    );
  }

  AppState copyWith(
      {ExampleProduct exampleProduct,
      UserUIState userUIState,
      DeviceInfo deviceInfo,
      List<Album> listAlbum,
      List<NotificationSlotState> listNotifications,
      UserInfoState userInfo,
      List<UserInfoState> friends
    })
    => AppState(
        exampleProduct: exampleProduct ?? this.exampleProduct,
        userUIState: userUIState ?? this.userUIState,
        deviceInfo: deviceInfo ?? this.deviceInfo,
        listAlbums: listAlbum ?? this.listAlbums,
        listNotifications: listNotifications ?? this.listNotifications,
        userInfo: userInfo ?? this.userInfo,
        friends: friends ?? this.friends
    );
}
