class UserUIState {
  final bool darkMode;

  UserUIState({this.darkMode});

  factory UserUIState.initial() {
    return UserUIState(darkMode: true);
  }

  UserUIState copyWith({
    bool darkMode,
  }) {
    return UserUIState(darkMode: darkMode ?? this.darkMode);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserUIState && darkMode == other.darkMode;

  @override
  int get hashCode => darkMode.hashCode;
}

// class ExampleState {
//   final List<ExampleProduct> orderProducts;

//   ExampleState({this.orderProducts});

//   factory ExampleState.initial() {
//     return ExampleState(orderProducts: new List<ExampleProduct>());
//   }

//   ExampleState copyWith({
//     List<ExampleProduct> orderProducts,
//   }) {
//     return ExampleState(orderProducts: orderProducts ?? this.orderProducts);
//   }

//   @override
//   bool operator ==(Object other) =>
//       identical(this, other) ||
//       other is ExampleState && DeepCollectionEquality().equals(this, other);

//   @override
//   int get hashCode => orderProducts.hashCode;
// }

class ExampleProduct {
  final String name;

  ExampleProduct({this.name});

  factory ExampleProduct.initial() {
    return ExampleProduct(name: "");
  }

  ExampleProduct copyWith({int id, int count, String name}) {
    return new ExampleProduct(name: name ?? this.name);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ExampleProduct && name == other.name;

  @override
  int get hashCode => name.hashCode;
}
