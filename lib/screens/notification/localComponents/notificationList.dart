import 'package:facebook/screens/notification/localComponents/notificationSlot.dart';
import 'package:facebook/stores/notificationState.dart';
import 'package:flutter/material.dart';

class NotificationList extends StatelessWidget {
  final List<Map<String, String>> data;
  final List<NotificationSlotState> dataNotify;
  NotificationList({@required this.data, @required this.dataNotify});
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return NotificationSlot(data: dataNotify[index]);
      },
      childCount: dataNotify.length,
    ));
  }
}