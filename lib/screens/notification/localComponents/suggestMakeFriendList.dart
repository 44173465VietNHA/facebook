import 'package:facebook/screens/notification/localComponents/suggestMakeFriendSlot.dart';
import 'package:flutter/material.dart';

class SuggestMakeFriendList extends StatelessWidget {
  final List<Map<String, String>> data;
  SuggestMakeFriendList({@required this.data});
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return SuggestMakeFriendSlot(data: data[index]);
      },
      childCount: data.length,
    ));
  }
}