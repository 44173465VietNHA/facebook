import 'dart:developer';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:facebook/screens/notification/notificationConst.dart';
import 'package:flutter/material.dart';

class SuggestMakeFriendSlot extends StatelessWidget {
  final Map<String, String> data;
  SuggestMakeFriendSlot({@required this.data});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: 60,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(8, 4, 5, 12),
            width: 40,
            child: ClipOval(
              child: CachedNetworkImage(
                imageUrl: data['imageUrl'],
                errorWidget: (context, url, error) => Icon(Icons.error),
                fit: BoxFit.cover,
                width: 40,
                height: 40,
              )
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 6),
                      alignment: Alignment.centerLeft,
                      child: Text(data['objectDo'], style: TextStyle(fontWeight: FontWeight.bold, fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text((data['isSend'] == '1') ? ' \u00B7 Đã gửi yêu cầu' : data['mutualFriends'], style: TextStyle(fontWeight: FontWeight.w300, fontSize: NOTIFICATION_FONT_SIZE_TIME))
                    )
                  ]
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: _buildBtnSlot(context, data['isSend'] == "1"),
                  )
                )
              ]
            )
          ),
        ]
      )
    );
  }
  List<Widget> _buildBtnSlot(BuildContext context, isSend) {
    if (isSend) {
      return [
        Expanded(
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 2, 10, 10),
            child: FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
              color: Color.fromRGBO(230, 230, 230, 1),
              textColor: Colors.black,
              onPressed: () => log("Tap cancel invite"),
              child: Text("Hủy", textAlign: TextAlign.center, style: TextStyle(fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
            )
          ),
        ),
      ];
    }
    return [
      Expanded(
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 2, 10, 10),
          child: FlatButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
            color: Color.fromRGBO(21, 134, 214, 1),
            textColor: Colors.white,
            onPressed: () => log("Tap invite friend"),
            child: Text("Thêm bạn bè", textAlign: TextAlign.center, style: TextStyle(fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
          )
        ),
      ),
      Expanded(
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 2, 10, 10),
          child: FlatButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
            color: Color.fromRGBO(230, 230, 230, 1),
            textColor: Colors.black,
            onPressed: () => log("Tap delete suggest"),
            child: Text("Xoá", textAlign: TextAlign.center, style: TextStyle(fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
          )
        )
      )
    ];
  }
}