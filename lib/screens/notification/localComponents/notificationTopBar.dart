import 'dart:developer';
import 'package:flutter/material.dart';

class NotificationTopBar extends StatelessWidget {
  NotificationTopBar();
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      primary: false,
      backgroundColor: Colors.white,
      floating: true,
      toolbarHeight: 30,       
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Text("Thông báo", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14))
          ),
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color.fromRGBO(0, 60, 190, 0.1),
            ),
            child: InkWell(
              onTap: () => log("Tap search"),
              customBorder: CircleBorder(),
              child: Container(
                width: 25,
                height: 25,
                child: Icon(Icons.search, color: Colors.black, size: 15)
              )
            )
          )
        ]
      ),
    );
  }
}