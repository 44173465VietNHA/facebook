import 'dart:developer';
import 'package:facebook/screens/notification/notificationConst.dart';
import 'package:flutter/material.dart';

class SuggestMakeFriendBottomBar extends StatelessWidget {
  SuggestMakeFriendBottomBar();
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 21,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
                color: Color.fromRGBO(230, 230, 230, 1),
                textColor: Colors.black,
                onPressed: () => log("Tap show all suggest make friend"),
                child: Text("Xem tất cả", textAlign: TextAlign.center, style: TextStyle(fontSize: NOTIFICATION_FONT_SIZE_CONTENT))
              )
            ),
            Container(
              margin: EdgeInsets.fromLTRB(16, 10, 0, 0),
              child: Divider(endIndent: 16, height: 1, thickness: 1)
            )
          ]
        );
      },
      childCount: 1,
    ));
  }
}