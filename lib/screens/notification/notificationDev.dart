import 'dart:developer';
import 'package:facebook/screens/notification/notificationScreen.dart';
import 'package:flutter/material.dart';
import 'notificationFakeData.dart';

//obsolete
class NotificationDev extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    log("Test NotificationDev");
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            // Container(height: 76),
            NotificationScreen(dataNotify: fakeNotify, dataSuggest: fakeSuggest),
          ]
        )
      )
    );
  }
}
