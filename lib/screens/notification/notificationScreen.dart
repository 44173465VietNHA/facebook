import 'package:facebook/screens/notification/localComponents/notificationTab.dart';
import 'package:facebook/screens/notification/notificationFakeData.dart';
import 'package:facebook/stores/notificationState.dart';
import 'package:flutter/material.dart';

class NotificationScreen extends StatelessWidget {
  final List<Map<String, String>> dataNotify;
  final List<Map<String, String>> dataSuggest;
  NotificationScreen({@required this.dataNotify, @required this.dataSuggest});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: NotificationTab(dataNotify: fakeNotify, dataSuggest: fakeSuggest)
    );
  }
}