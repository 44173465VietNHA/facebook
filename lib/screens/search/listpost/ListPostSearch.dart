import 'dart:ffi';

import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/screens/search/listpost/SinglePostSearch.dart';
import 'package:flutter/material.dart';

class ListPostSearch extends StatefulWidget {
  final List<SearchData> data;
  ListPostSearch(@required this.data);
  State<ListPostSearch> s;
  void refresh() {
    s.setState(() {});
  }

  Lax createState() {
    s = Lax(data);
    return s;
  }
}

class Lax extends State<ListPostSearch> {
  List<SearchData> data;

  @override
  void initState() {
    super.initState();
  }

  Lax(@required this.data);

  final GlobalKey<Lax> _scaffoldKey = GlobalKey<Lax>();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // key: _scaffoldKey,
      // key: Key(data.length.toString()),
      itemCount: data.length,
      // primary: false,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.only(top: 0),
      itemBuilder: (BuildContext context, int index) {
          return SinglePostSearch(data[index]);
      },
    );
  }
}
