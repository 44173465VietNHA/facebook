import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/api/search/Search.dart';
import 'package:facebook/screens/search/listpost/SearchFuture.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SearchPost extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;
  Future<SearchResponse> searchResponse;
  String token;
  String authorization;
  String keywork;
  int user_id;
  int index;
  int count;
  TextEditingController searchField;
  SearchFuture searchFuture;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchField = TextEditingController();
    token = Constants.token;
    authorization = Constants.token;
    // keywork = "moi";
    user_id = Constants.user_id;
    index = 0;
    count = 0;
    searchFuture = SearchFuture(searchResponse: searchResponse);
    // searchResponse = search(token, token, keywork, user_id, index, count)
    //     as Future<SearchResponse>;
    // Create TabController for getting the index of current tab

    // searchField.addListener(() {
    //   setState(() {
    //   });
    // });
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  void dispose() {
    searchField.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 5, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              bottom: PreferredSize(
                child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.grey,
                  labelColor: Colors.blue,
                  tabs: [
                    Tab(
                      child: Text("Tất cả"),
                    ),
                    Tab(
                      child: Text("Bài viết"),
                    ),
                    Tab(
                      child: Text("Mọi người"),
                    ),
                    Tab(
                      child: Text("Ảnh"),
                    ),
                    Tab(
                      child: Text("Video"),
                    ),
                  ],
                ),
                preferredSize: Size.fromHeight(40.0),
              ),
              backgroundColor: Colors.white,
              title: new Container(
                child: new TextField(
                  controller: searchField,
                  textAlign: TextAlign.left,
                  // autofocus: true,
                  style: TextStyle(fontSize: 22.0, color: Colors.black),
                  // onSubmitted: searchEvent(searchField.text),
                  // onSubmitted: searchEvent(searchField.text),
                  onSubmitted: (String value) async {
                    var sa = await search(token, token, value, user_id, index, count);
                    setState(() => searchFuture.s.searchResponseS = sa
                        );
                    searchFuture.reload();
                  },
                  textInputAction: TextInputAction.search,
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    // suffixIcon: (searchField.text.length > 0)?
                    suffixIcon: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.close),
                        onPressed: () {
                          searchField.text = '';
                        }),
                    // : null,
                    filled: true,
                    fillColor: Color.fromRGBO(240, 242, 245, 1),
                    hintText: 'Tìm kiếm',
                    contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                  ),
                ),
                padding: EdgeInsets.only(top: 8),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {
                        Navigator.pop(context);
                      }))),
          
          body: new TabBarView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          'Mọi người',
                          textScaleFactor: 1.5,
                          style: TextStyle(color: Colors.black),
                        ),
                        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                ),
                // Center(
                //   child: Text('Tất cả'),
                // ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              SingleChildScrollView(
                child: searchFuture,
              ),
              Container(
                child: Center(
                  child: Text('Mọi người'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Ảnh'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Video'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
