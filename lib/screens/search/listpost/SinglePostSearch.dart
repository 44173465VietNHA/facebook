import 'dart:math';

import 'package:chewie/chewie.dart';
import 'package:facebook/api/like/like.dart';
import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/api/post/delete_post.dart';
import 'package:facebook/screens/comment/Comment.dart';
import 'package:facebook/screens/comment/Comment.dart' as Pass;
import 'package:facebook/screens/singlepost/singlepost.dart';
import 'package:facebook/screens/videoblack/videoblack.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:facebook/router/Router.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path/path.dart' as p;
import 'package:video_player/video_player.dart';

class SinglePostSearch extends StatefulWidget {
  final SearchData data;
  SinglePostSearch(@required this.data);

  @override
  _DescriptionTextWidgetState createState() =>
      new _DescriptionTextWidgetState(data);
}

class _DescriptionTextWidgetState extends State<SinglePostSearch> {
  SearchData data;
  _DescriptionTextWidgetState(@required this.data);
  String firstHalf;
  String secondHalf;
  bool flag = true;
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;
  Chewie player;
  // final GlobalKey<_DescriptionTextWidgetState> _scaffoldKey = GlobalKey<_DescriptionTextWidgetState>();
  @override
  void initState() {
    super.initState();
    if (data.content == null) {
      firstHalf = "";
      secondHalf = "";
    } else if (data.content.length > 200) {
      firstHalf = data.content.substring(0, 200);
      secondHalf = data.content.substring(200, data.content.length);
    } else {
      firstHalf = data.content;
      secondHalf = "";
    }

    if (data.media != null &&
        data.media.length >= 1 &&
        p.extension(data.media[0].fileName) == ".mp4") {
      print(Constants.FileURI + data.media[0].fileName);
      _videoPlayerController = VideoPlayerController.network(
          Constants.FileURI + data.media[0].fileName);
      _chewieController =
          // ChewieController(
          //   videoPlayerController: _videoPlayerController,
          //   aspectRatio: _videoPlayerController.value.aspectRatio,
          //   autoPlay: false,
          //   looping: false,
          // );
          ChewieController(
        videoPlayerController: _videoPlayerController,
        aspectRatio: _videoPlayerController.value.aspectRatio * 0.6,
        autoInitialize: true,
        // showControlsOnInitialize: false,
        autoPlay: true,
        allowFullScreen: true,
        fullScreenByDefault: false,
        deviceOrientationsAfterFullScreen: [
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown
        ],
        systemOverlaysAfterFullScreen: SystemUiOverlay.values,
        showControls: false,
      );
      // _chewieController.enterFullScreen();
      player = Chewie(controller: _chewieController);
    }
  }

  // @override
  // void dispose() {
  //   _videoPlayerController.dispose();
  //   _chewieController.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleAvatar(
                      radius: 20,
                      backgroundImage: (data.author.avatar != null)
                          ? NetworkImage(Constants.FileURI + data.author.avatar)
                          : NetworkImage(
                              'https://1.bp.blogspot.com/-m3UYn4_PEms/Xnch6mOTHJI/AAAAAAAAZkE/GuepXW9p7MA6l81zSCnmNaFFhfQASQhowCLcBGAsYHQ/s1600/Cach-Lam-Avatar-Dang-Hot%2B%25281%2529.jpg')),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: RichText(
                          text: new TextSpan(
                            children: [
                              new TextSpan(
                                  text: data.author.name,
                                  style: new TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () {
                                      print("@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                      print(data.author.id);
                                      Navigator.pushNamed(
                                          context, PERSONAL_PAGE,
                                          arguments: {"id": data.author.id});
                                    }),
                              // new TextSpan(
                              //   text: ' ở ',
                              //   style: new TextStyle(color: Colors.black),
                              // ),
                              // new TextSpan(
                              //   text: 'Thiết kế Nhà Việt',
                              //   style: new TextStyle(
                              //     color: Colors.black,
                              //     fontWeight: FontWeight.bold,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.only(top: 5),
                                child: RichText(
                                  text: new TextSpan(
                                    children: [
                                      new TextSpan(
                                        text: data.editTime,
                                        style: new TextStyle(
                                          color:
                                              Color.fromRGBO(130, 130, 130, 1),
                                          fontSize: 12,
                                        ),
                                      ),
                                      new TextSpan(
                                        // text: ' * Thành phố Hồ Chí Minh',
                                        text: '',
                                        style: new TextStyle(
                                          color:
                                              Color.fromRGBO(130, 130, 130, 1),
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              CircleAvatar(
                                  radius: 10,
                                  backgroundImage:
                                      NetworkImage(Constants.fakeImage)),
                            ]
                            ),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Text(""),
                  ),
                  new Align(
                    alignment: Alignment.bottomRight,
                    widthFactor: 0.85,
                    child: Container(
                      // padding: ,
                      child: Sheet(data),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: RichText(
                text: new TextSpan(
                  children: [
                    new TextSpan(
                      text: secondHalf != ""
                          ? flag
                              ? (firstHalf + "...")
                              : (firstHalf + secondHalf)
                          : firstHalf,
                      style: new TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    if (data.content != null && data.content.length > 200)
                      new TextSpan(
                        text: flag ? "Xem thêm" : "",
                        style: new TextStyle(
                          color: Color.fromRGBO(130, 130, 130, 1),
                          fontWeight: FontWeight.bold,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            setState(() {
                              flag = !flag;
                            });
                          },
                      ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: data.media != null && data.media.length >= 1
                  ? p.extension(data.media[0].fileName) != ".mp4"
                      ?
                      // data.media.
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (data.media.length >= 1)
                              InkWell(
                                onTap: () async {
                                  DataPass dataPass =
                                      await showSinglePost(context, data);
                                  setState(() {
                                    data.is_liked = dataPass.pass_is_liked;
                                    data.liked = dataPass.pass_liked;
                                    data.comment = dataPass.comment;
                                  });
                                },
                                child: Image.network(
                                    Constants.FileURI + data.media[0].fileName,
                                    height:
                                        (105 * max(1, 3 / data.media.length))
                                            .toDouble(),
                                    width: (105 * max(1, 3 / data.media.length))
                                        .toDouble()),
                              ),
                            if (data.media.length == 2)
                              InkWell(
                                onTap: () async {
                                  DataPass dataPass =
                                      await showSinglePost(context, data);
                                  setState(() {
                                    data.is_liked = dataPass.pass_is_liked;
                                    data.liked = dataPass.pass_liked;
                                    data.comment = dataPass.comment;
                                  });
                                },
                                child: Image.network(
                                    Constants.FileURI + data.media[1].fileName,
                                    height: (105 * 1.5).toDouble(),
                                    width: (105 * 1.5).toDouble()),
                              ),
                            if (data.media.length == 3)
                              InkWell(
                                onTap: () async {
                                  DataPass dataPass =
                                      await showSinglePost(context, data);
                                  setState(() {
                                    data.is_liked = dataPass.pass_is_liked;
                                    data.liked = dataPass.pass_liked;
                                    data.comment = dataPass.comment;
                                  });
                                },
                                child: Image.network(
                                  Constants.FileURI + data.media[2].fileName,
                                  height: 105,
                                  width: 105,
                                ),
                              ),
                            if (data.media.length >= 4)
                              InkWell(
                                onTap: () async {
                                  DataPass dataPass =
                                      await showSinglePost(context, data);
                                  setState(() {
                                    data.is_liked = dataPass.pass_is_liked;
                                    data.liked = dataPass.pass_liked;
                                    data.comment = dataPass.comment;
                                  });
                                },
                                child: Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      ColorFiltered(
                                        colorFilter: ColorFilter.mode(
                                          Colors.grey.withOpacity(0.85),
                                          BlendMode.darken,
                                        ),
                                        child: Image.network(
                                          Constants.FileURI +
                                              data.media[2].fileName,
                                          height: 105,
                                          width: 105,
                                        ),
                                      ),
                                      Text(
                                        (data.media.length - 3).toString() +
                                            "+",
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ]),
                              ),
                          ],
                        )
                      : InkWell(
                          onTap: () {
                            // print("_____________####_______________" + );
                            Navigator.pushNamed(context, BLACK_VIDEO,
                                arguments: {
                                  "id": data.id
                                });
                            // print();
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => VideoBlack(data)));
                          },
                          child: player,
                        )
                  //Thêm video vào đây
                  // :null
                  : null,
            ),
            InkWell(
              onTap: () async {
                DataPass dataPass = await showComment(
                    context, data.liked, data.is_liked, data.comment, data.id);
                setState(() {
                  data.is_liked = dataPass.pass_is_liked;
                  data.liked = dataPass.pass_liked;
                  data.comment = dataPass.comment;
                });
              },
              child: Container(
                  padding: EdgeInsets.fromLTRB(18, 0, 15, 0),
                  child: Row(
                    children: [
                      data.liked != 0
                          ? Row(
                              children: [
                                CircleAvatar(
                                  radius: 6,
                                  backgroundImage:
                                      AssetImage("assets/Icon/17.jpg"),
                                ),
                                Text(data.liked.toString() + "  thích"),
                              ],
                            )
                          : Container(),
                      Expanded(
                        child: Text(""),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Row(
                          children: [
                            Text(data.comment.toString() + " bình luận"),
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 18, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  data.is_liked == "false"
                      ? InkWell(
                          onTap: () {
                            setState(() {
                              data.is_liked = "true";
                              data.liked = data.liked + 1;
                              like(Constants.authorization, Constants.token,
                                  data.id);
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.all(18),
                            child: Row(
                              children: [
                                Icon(
                                  MdiIcons.thumbUpOutline,
                                  size: 16,
                                ),
                                Text("  Thích"),
                              ],
                            ),
                          ))
                      : InkWell(
                          onTap: () {
                            setState(() {
                              data.is_liked = "false";
                              data.liked = data.liked - 1;
                              like(Constants.authorization, Constants.token,
                                  data.id);
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.all(18),
                            child: Row(
                              children: [
                                Icon(
                                  MdiIcons.thumbUp,
                                  size: 16,
                                  color: Color.fromRGBO(23, 112, 227, 1),
                                ),
                                Text("  Thích",
                                    style: TextStyle(
                                        color:
                                            Color.fromRGBO(23, 112, 227, 1))),
                              ],
                            ),
                          ),
                        ),
                  InkWell(
                    onTap: () async {
                      DataPass dataPass = await showComment(context, data.liked,
                          data.is_liked, data.comment, data.id);
                      setState(() {
                        data.is_liked = dataPass.pass_is_liked;
                        data.liked = dataPass.pass_liked;
                        data.comment = dataPass.comment;
                      });

                      // .then((){
                      //   setState(() {
                      //   data.is_liked = Pass.pass_is_liked;
                      //   data.liked = Pass.pass_liked;
                      // });
                      // });
                    },
                    child: Container(
                      padding: EdgeInsets.all(18),
                      child: Row(
                        children: [
                          Icon(
                            MdiIcons.commentOutline,
                            size: 16,
                          ),
                          Text("  Bình luận"),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Icon(
                        MdiIcons.shareOutline,
                        size: 18,
                      ),
                      Text("  Chia sẻ"),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          // borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
      ),
      color: Color.fromRGBO(201, 204, 209, 1),
    );
  }
}

class Sheet extends StatelessWidget {
  final SearchData data;
  const Sheet(@required this.data);
  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
        color: Colors.white,
        padding: EdgeInsets.all(0),
        elevation: 0,
        child: Container(
          color: Colors.white,
          child: Icon(Icons.more_horiz),
          // padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
        ),
        onPressed: () {
          // Show sheet here
          showModalBottomSheet<void>(
              context: context,
              builder: (BuildContext context) {
                return new Container(
                  // decoration: BoxDecoration(
                  //   borderRadius: BorderRadius.circular(20),
                  //   color: Colors.white,
                  //   boxShadow: [
                  //     BoxShadow(color: Colors.white, spreadRadius: 7),
                  //   ],
                  // ),
                  child: new Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () => messenger(data.id),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Row(
                              children: [
                                Image.network(
                                  Constants.fakeImage,
                                  width: 32,
                                  height: 32,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    'Chỉnh sửa bài viết',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            deletePost(data);
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Row(
                              children: [
                                Image.network(
                                  Constants.fakeImage,
                                  width: 32,
                                  height: 32,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    'Xóa bài viết',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              });
        });
  }

  void blockUser(int id) {}

  void messenger(int id) {
    print("messager user_id: ");
    print(id);
  }

  void deletePost(SearchData data) {
    Constants.homeState.s.deleteData(data);
    delete_post(Constants.authorization, Constants.token, data.id);
  }
}
