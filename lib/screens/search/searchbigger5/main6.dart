import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyApp6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              title: new Container(
                child: new TextField(
                  // obscuringCharacter: "Linh",
                  textAlign: TextAlign.left,
                  autofocus: false,
                  style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color.fromRGBO(240, 242, 245, 1),
                    hintText: 'Tìm kiếm',
                    contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                  ),
                ),
                padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {}))),
          body: new TabBarView(
            children: <Widget>[
              // SingleChildScrollView(
                Container(
                  padding: EdgeInsets.fromLTRB(0, 2, 0, 0),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Center(
                            child: Container(
                              padding: EdgeInsets.fromLTRB(40, 40, 40, 0),
                              child: Column(
                                children: [
                                  Image.network(
                                      'https://scontent-hkt1-1.xx.fbcdn.net/v/t1.15752-9/122292489_1025398381260372_6362993524506115218_n.png?_nc_cat=107&ccb=2&_nc_sid=ae9488&_nc_ohc=43UPhtNaeTQAX9o_xmT&_nc_ht=scontent-hkt1-1.xx&oh=f28f86c76e9370bb8e30dc96e2636436&oe=5FB89CDF',
                                    width: 100,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 20),
                                    child: Center(child: Text("Hãy nhập vài từ để tìm kiếm trong Facebook",
                                      textAlign: TextAlign.center,)),
                                  )

                                ],
                              ),
                            ),
                          ),
                          padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(0),
                      color: Colors.white,
                    ),
                  ),
                  color: Color.fromRGBO(201, 204, 209, 1),
                ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
