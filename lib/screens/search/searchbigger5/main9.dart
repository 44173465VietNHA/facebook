import 'package:flutter/material.dart';

class MyApp9 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 5, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              bottom: PreferredSize(
                child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.grey,
                  labelColor: Colors.blue,
                  tabs: [
                    Tab(
                      child: Text("Tất cả"),
                    ),
                    Tab(
                      child: Text("Bài viết"),
                    ),
                    Tab(
                      child: Text("Mọi người"),
                    ),
                    Tab(
                      child: Text("Ảnh"),
                    ),
                    Tab(
                      child: Text("Video"),
                    ),
                  ],
                ),
                preferredSize: Size.fromHeight(40.0),
              ),
              backgroundColor: Colors.white,
              // actions: <Widget>[
              //   new IconButton(
              //     color: Colors.black,
              //     icon: new Icon(Icons.close),
              //     onPressed: () {},
              //   ),
              // ],
              title: new Container(
                child: new TextField(
                  // obscuringCharacter: "Linh",
                  textAlign: TextAlign.left,
                  autofocus: false,
                  style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    suffixIcon: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.close),
                        onPressed: () {}),
                    filled: true,
                    fillColor: Color.fromRGBO(240, 242, 245, 1),
                    hintText: 'Linh',
                    contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                  ),
                ),
                padding: EdgeInsets.only(top: 8),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {}))),
          body: new TabBarView(
            children: <Widget>[
          SingleChildScrollView(
              child: Column(
              children : [
                Container(
                  padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            'Mọi người',
                            textScaleFactor: 1.5,
                            style: TextStyle(color: Colors.black),
                          ),
                          padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color: Color.fromRGBO(201, 204, 209, 1),
                                )
                            ),
                          ),
                          padding: EdgeInsets.fromLTRB(7, 10, 0, 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CircleAvatar(
                                  radius: 30,
                                  backgroundImage: NetworkImage(
                                      'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-1/p200x200/1474578_578859478854425_2031910317_n.jpg?_nc_cat=105&_nc_sid=7206a8&_nc_ohc=uXywb24Rpy4AX9G-FYd&_nc_ht=scontent.fhan2-4.fna&tp=6&oh=646907ebf33647f97178c03f5d589721&oe=5FB6C477')),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 12, 10, 2),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Nguyễn Tuấn Linh",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text("Ebolic - Ebooks for Bookaholic"),
                                    ]),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 10, 10, 2),
                                child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 19,
                                    backgroundImage: NetworkImage(
                                        'https://scontent.fhan2-1.fna.fbcdn.net/v/t1.15752-9/cp0/122095612_654357241934913_4037696545272729314_n.png?_nc_cat=101&_nc_sid=ae9488&_nc_ohc=lPCEwh4DHcAAX82_Oal&_nc_ht=scontent.fhan2-1.fna&oh=a5aa7c11f4c21c9ad16d94c1072eb98b&oe=5FB73749')),
                              ),
                            ],
                          ),
                        ),


                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color: Color.fromRGBO(201, 204, 209, 1),
                                )
                            ),
                          ),
                          padding: EdgeInsets.fromLTRB(7, 10, 0, 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CircleAvatar(
                                  radius: 30,
                                  backgroundImage: NetworkImage(
                                      'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/119451208_2657448327809497_5954524205515006888_n.jpg?_nc_cat=108&ccb=1&_nc_sid=09cbfe&_nc_ohc=A9FCHBZT30gAX-rHzR6&_nc_ht=scontent.fhan2-3.fna&oh=2b92cd957fbda5cad46b5c3c3f34f55d&oe=5FB440DF')),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 12, 10, 2),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Hà Linh Nguyễn",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text("Ebolic - Ebooks for Bookaholic"),
                                    ]),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 10, 10, 2),
                                child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 19,
                                    backgroundImage: NetworkImage(
                                        'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.15752-9/cp0/122447029_661149528139617_4873259118860701884_n.png?_nc_cat=110&ccb=1&_nc_sid=ae9488&_nc_ohc=1cC7YOvDUysAX_31jkt&_nc_ht=scontent.fhan2-4.fna&oh=1ae9e539f260e7ab516295b242ff481c&oe=5FB7E819')),
                              ),
                            ],
                          ),
                        ),


                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                  color: Color.fromRGBO(201, 204, 209, 1),
                                )
                            ),
                          ),
                          padding: EdgeInsets.fromLTRB(7, 10, 0, 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CircleAvatar(
                                  radius: 30,
                                  backgroundImage: NetworkImage(
                                      'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-1/p200x200/1474578_578859478854425_2031910317_n.jpg?_nc_cat=105&_nc_sid=7206a8&_nc_ohc=uXywb24Rpy4AX9G-FYd&_nc_ht=scontent.fhan2-4.fna&tp=6&oh=646907ebf33647f97178c03f5d589721&oe=5FB6C477')),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 12, 10, 2),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Nguyễn Tuấn Linh",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text("Ebolic - Ebooks for Bookaholic"),
                                    ]),
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(15, 10, 10, 2),
                                child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 19,
                                    backgroundImage: NetworkImage(
                                        'https://scontent.fhan2-1.fna.fbcdn.net/v/t1.15752-9/cp0/122095612_654357241934913_4037696545272729314_n.png?_nc_cat=101&_nc_sid=ae9488&_nc_ohc=lPCEwh4DHcAAX82_Oal&_nc_ht=scontent.fhan2-1.fna&oh=a5aa7c11f4c21c9ad16d94c1072eb98b&oe=5FB73749')),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white,
                    ),
                  ),
                  color: Color.fromRGBO(201, 204, 209, 1),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.network(
                        'https://scontent.fhan2-5.fna.fbcdn.net/v/t1.15752-9/122425822_782301979226153_2444495438980758565_n.png?_nc_cat=107&ccb=2&_nc_sid=ae9488&_nc_ohc=pYcCmv3HejAAX_BM2pE&_nc_ht=scontent.fhan2-5.fna&oh=1a3a9dac80ded0895d617ad00fa7644b&oe=5FB83E86',
                        width: 200,
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Text('Không thể tải kết quả'),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          //Center Column contents vertically,
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.refresh),
                            Text('Thử lại')
                          ],
                        ),
                      ),
                    ],
                  ),
                  color: Color.fromRGBO(201, 204, 209, 1),
                )
              ]
          )

          ),
              Container(
                child: Center(
                  child: Text('Bài viết'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Mọi người'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Ảnh'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              Container(
                child: Center(
                  child: Text('Video'),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
