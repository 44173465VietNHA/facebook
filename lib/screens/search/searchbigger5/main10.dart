import 'package:flutter/material.dart';

class MyApp10 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              title: new Container(
                child: new TextField(
                  // obscuringCharacter: "Linh",
                  textAlign: TextAlign.left,
                  autofocus: false,
                  style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    suffixIcon: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.close),
                        onPressed: () {}),
                    filled: true,
                    fillColor: Color.fromRGBO(240, 242, 245, 1),
                    hintText: '#goc_chia_se',
                    contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                  ),
                ),
                padding: EdgeInsets.only(top: 8),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {}))),
          body: new TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 2, 0, 0),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: RichText(
                                          text: new TextSpan(
                                            children: [
                                              new TextSpan(
                                                text: '#goc_chia_se',
                                                style: new TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                ),
                                                // recognizer: new new InkWell(
                                                //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                //   },
                                              ),
                                              new TextSpan(
                                                text: '',
                                                style: new TextStyle(
                                                    color: Colors.black),
                                              ),
                                              new TextSpan(
                                                text: '',
                                                style: new TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                // recognizer: new new InkWell(
                                                //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                //   },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 10),
                                        child: Row(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding:
                                                EdgeInsets.only(top: 5),
                                                child: RichText(
                                                  text: new TextSpan(
                                                    children: [
                                                      new TextSpan(
                                                        text: 'Khám phá',
                                                        style: new TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontSize: 12,
                                                        ),
                                                        // recognizer: new new InkWell(
                                                        //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                        //   },
                                                      ),
                                                      new TextSpan(
                                                        text:
                                                        '',
                                                        style: new TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            //   CircleAvatar(
                                            //       radius: 10,
                                            //       backgroundImage: NetworkImage(
                                            //           'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/cp0/122545019_2771691493076404_8146798766004960948_n.png?_nc_cat=108&ccb=2&_nc_sid=ae9488&_nc_ohc=SiDzttBqgKoAX9yLBKh&_nc_ht=scontent.fhan2-3.fna&oh=2b7f1e0a31c914c77acf175c08bcf40b&oe=5FBA2722')),
                                            ]),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: Text(""),
                                  ),
                                  new Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0.0),
                                      child: Icon(Icons.more_horiz),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white,
                        ),
                      ),
                      // Center(
                      //   child: Text('Tất cả'),
                      // ),
                      color: Color.fromRGBO(201, 204, 209, 1),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CircleAvatar(
                                      radius: 20,
                                      backgroundImage: NetworkImage(
                                          'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-1/p200x200/1474578_578859478854425_2031910317_n.jpg?_nc_cat=105&_nc_sid=7206a8&_nc_ohc=uXywb24Rpy4AX9G-FYd&_nc_ht=scontent.fhan2-4.fna&tp=6&oh=646907ebf33647f97178c03f5d589721&oe=5FB6C477')),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 10),
                                        child: RichText(
                                          text: new TextSpan(
                                            children: [
                                              new TextSpan(
                                                text: 'Hani Hani',
                                                style: new TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                // recognizer: new new InkWell(
                                                //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                //   },
                                              ),
                                              new TextSpan(
                                                text: ' > ',
                                                style: new TextStyle(
                                                    color: Colors.black),
                                              ),
                                              new TextSpan(
                                                text: 'Nhóm giảm cân KETO',
                                                style: new TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                // recognizer: new new InkWell(
                                                //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                //   },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 10),
                                        child: Row(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding:
                                                EdgeInsets.only(top: 5),
                                                child: RichText(
                                                  text: new TextSpan(
                                                    children: [
                                                      new TextSpan(
                                                        text: '5 ngày * ',
                                                        style: new TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontSize: 12,
                                                        ),
                                                        // recognizer: new new InkWell(
                                                        //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                        //   },
                                                      ),
                                                      new TextSpan(
                                                        text:
                                                        '',
                                                        style: new TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              CircleAvatar(
                                                  radius: 10,
                                                  backgroundImage: NetworkImage(
                                                      'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/cp0/122545019_2771691493076404_8146798766004960948_n.png?_nc_cat=108&ccb=2&_nc_sid=ae9488&_nc_ohc=SiDzttBqgKoAX9yLBKh&_nc_ht=scontent.fhan2-3.fna&oh=2b7f1e0a31c914c77acf175c08bcf40b&oe=5FBA2722')),
                                            ]),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: Text(""),
                                  ),
                                  new Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0.0),
                                      child: Icon(Icons.more_horiz),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                              child: RichText(
                                text: new TextSpan(
                                  children: [
                                    new TextSpan(
                                      text: '#Góc_Chia_Sẻ\n',
                                      style: new TextStyle(
                                        color: Colors.blue,
                                      ),
                                    ),
                                    new TextSpan(
                                      text:
                                      '''Tại sao iPhone lại không có nút từ chối cuộc gọi?
Nếu là người dùng iPhone, có lẽ bạn đã nhận thấy khi bạn nhận được cuộc gọi đến, đôi khi bạn thấy nút Decline (Từ chối), và có lúc bạn lại không thấy nó đâu. Đôi lúc, việc thiếu nút từ chối cũng gây khó chịu cho người dùng (trong đó có mình).
Vậy điều gì lại khiến nó như vậy và tại sao Apple lại tạo ra tính năng khó hiểu này? Thật ra, điều này là do lúc bạn mở hay khóa màn hình điện thoại. .
Cụ thể, nếu điện thoại của bạn được mở… ''',
                                      style: new TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: 'Xem thêm',
                                      style: new TextStyle(
                                        color: Color.fromRGBO(130, 130, 130, 1),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.network(
                                        'https://nhaxinhcenter.com.vn/source/pic/biet-thu-co-dien-dong-nai.jpg',
                                        height: 105,
                                        width: 105),
                                    Image.network(
                                        'https://cms.luatvietnam.vn/uploaded/Images/Original/2019/03/04/thuc-hanh-tiet-kiem-chong-lang-phi-2019_0403145346.jpg',
                                        height: 105,
                                        width: 105),
                                    Stack(
                                        alignment: Alignment.center,
                                        children: <Widget>[
                                          ColorFiltered(
                                            colorFilter: ColorFilter.mode(
                                              Colors.grey.withOpacity(0.85),
                                              BlendMode.darken,
                                            ),
                                            child: Image.network(
                                              'https://hanoiflycam.com/wp-content/uploads/2017/03/31836222105_61c21fbc2f_o.jpg',
                                              height: 105,
                                              width: 105,
                                            ),
                                          ),
                                          Text(
                                            "+3",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ]),
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        CircleAvatar(
                                            radius: 10,
                                            backgroundImage: NetworkImage(
                                                'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.15752-9/cp0/122265727_386138892423517_7180594626603905199_n.png?_nc_cat=110&ccb=2&_nc_sid=ae9488&_nc_ohc=lRG-PWtH0XkAX_9B7qx&_nc_ht=scontent.fhan2-4.fna&oh=31b45f16d3577f165c28568bb1229e5f&oe=5FB80DF9'                                            )
                                        ),
                                        Text("  Thích"),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CircleAvatar(
                                            radius: 10,
                                            backgroundImage: NetworkImage(
                                                'https://scontent.fhan2-5.fna.fbcdn.net/v/t1.15752-9/cp0/122147420_2659553444335003_1533871656299437404_n.png?_nc_cat=107&ccb=2&_nc_sid=ae9488&_nc_ohc=ileoeK2sViYAX8mlOJL&_nc_ht=scontent.fhan2-5.fna&oh=a6d5abafbb66015dcb4d9b6b8117cf5a&oe=5FB82650'                                            )
                                        ),
                                        Text("  Bình luận"),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CircleAvatar(
                                            radius: 10,
                                            backgroundImage: NetworkImage(
                                                'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/cp0/122623521_777472969651832_3545550980996283940_n.png?_nc_cat=108&ccb=2&_nc_sid=ae9488&_nc_ohc=EsQfzHpKrcYAX8JCXBL&_nc_ht=scontent.fhan2-3.fna&oh=f53957d2f063d392295d6da43b53e0b4&oe=5FBB9ADA'                                            )
                                        ),
                                        Text("  Chia sẻ"),
                                      ],
                                    ),
                                  ],
                                ))
                          ],
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                      ),
                      // Center(
                      //   child: Text('Tất cả'),
                      // ),
                      color: Color.fromRGBO(201, 204, 209, 1),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CircleAvatar(
                                      radius: 20,
                                      backgroundImage: NetworkImage(
                                          'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/90791271_3107297682665702_3597589822606147584_o.png?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=0WyIlDZi5DMAX8-FiNK&_nc_ht=scontent.fhan2-2.fna&oh=29d4ef23f205eae5858d09f467fe7d6c&oe=5FBA575C'
                                      )
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 10),
                                        child: RichText(
                                          text: new TextSpan(
                                            children: [
                                              new TextSpan(
                                                text: 'Cooky Việt Nam',
                                                style: new TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                // recognizer: new new InkWell(
                                                //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                //   },
                                              ),
                                              new TextSpan(
                                                text: '',
                                                style: new TextStyle(
                                                    color: Colors.black),
                                              ),
                                              new TextSpan(
                                                text: '',
                                                style: new TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                                // recognizer: new new InkWell(
                                                //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                //   },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                        EdgeInsets.fromLTRB(10, 0, 10, 10),
                                        child: Row(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding:
                                                EdgeInsets.only(top: 5),
                                                child: RichText(
                                                  text: new TextSpan(
                                                    children: [
                                                      new TextSpan(
                                                        text: '30 thg 7 * ',
                                                        style: new TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontSize: 12,
                                                        ),
                                                        // recognizer: new new InkWell(
                                                        //   ..onTap = () { launch('https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                                                        //   },
                                                      ),
                                                      new TextSpan(
                                                        text:
                                                        '',
                                                        style: new TextStyle(
                                                          color: Color.fromRGBO(
                                                              130, 130, 130, 1),
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              CircleAvatar(
                                                  radius: 10,
                                                  backgroundImage: NetworkImage(
                                                      'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/cp0/122545019_2771691493076404_8146798766004960948_n.png?_nc_cat=108&ccb=2&_nc_sid=ae9488&_nc_ohc=SiDzttBqgKoAX9yLBKh&_nc_ht=scontent.fhan2-3.fna&oh=2b7f1e0a31c914c77acf175c08bcf40b&oe=5FBA2722')),
                                            ]),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: Text(""),
                                  ),
                                  new Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0.0),
                                      child: Icon(Icons.more_horiz),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                              child: RichText(
                                text: new TextSpan(
                                  children: [
                                    new TextSpan(
                                      text:
                                      '''Tại sao iPhone lại không có nút từ chối cuộc gọi?
Nếu là người dùng iPhone, có lẽ bạn đã nhận thấy khi bạn nhận được cuộc gọi đến, đôi khi bạn thấy nút Decline (Từ chối), và có lúc bạn lại không thấy nó đâu. Đôi lúc, việc thiếu nút từ chối cũng gây khó chịu cho người dùng (trong đó có mình).
Vậy điều gì lại khiến nó như vậy và tại sao Apple lại tạo ra tính năng khó hiểu này? Thật ra, điều này là do lúc bạn mở hay khóa màn hình điện thoại. .
Cụ thể, nếu điện thoại của bạn được mở… ''',
                                      style: new TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: 'Xem thêm',
                                      style: new TextStyle(
                                        color: Color.fromRGBO(130, 130, 130, 1),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.network(
                                        'https://nhaxinhcenter.com.vn/source/pic/biet-thu-co-dien-dong-nai.jpg',
                                        height: 105,
                                        width: 105),
                                    Image.network(
                                        'https://cms.luatvietnam.vn/uploaded/Images/Original/2019/03/04/thuc-hanh-tiet-kiem-chong-lang-phi-2019_0403145346.jpg',
                                        height: 105,
                                        width: 105),
                                    Stack(
                                        alignment: Alignment.center,
                                        children: <Widget>[
                                          ColorFiltered(
                                            colorFilter: ColorFilter.mode(
                                              Colors.grey.withOpacity(0.85),
                                              BlendMode.darken,
                                            ),
                                            child: Image.network(
                                              'https://hanoiflycam.com/wp-content/uploads/2017/03/31836222105_61c21fbc2f_o.jpg',
                                              height: 105,
                                              width: 105,
                                            ),
                                          ),
                                          Text(
                                            "+3",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ]),
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.fromLTRB(20, 0, 20, 15),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        CircleAvatar(
                                            radius: 10,
                                            backgroundImage: NetworkImage(
                                                'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.15752-9/cp0/122265727_386138892423517_7180594626603905199_n.png?_nc_cat=110&ccb=2&_nc_sid=ae9488&_nc_ohc=lRG-PWtH0XkAX_9B7qx&_nc_ht=scontent.fhan2-4.fna&oh=31b45f16d3577f165c28568bb1229e5f&oe=5FB80DF9'                                            )
                                        ),
                                        Text("  Thích"),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CircleAvatar(
                                            radius: 10,
                                            backgroundImage: NetworkImage(
                                                'https://scontent.fhan2-5.fna.fbcdn.net/v/t1.15752-9/cp0/122147420_2659553444335003_1533871656299437404_n.png?_nc_cat=107&ccb=2&_nc_sid=ae9488&_nc_ohc=ileoeK2sViYAX8mlOJL&_nc_ht=scontent.fhan2-5.fna&oh=a6d5abafbb66015dcb4d9b6b8117cf5a&oe=5FB82650'                                            )
                                        ),
                                        Text("  Bình luận"),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        CircleAvatar(
                                            radius: 10,
                                            backgroundImage: NetworkImage(
                                                'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.15752-9/cp0/122623521_777472969651832_3545550980996283940_n.png?_nc_cat=108&ccb=2&_nc_sid=ae9488&_nc_ohc=EsQfzHpKrcYAX8JCXBL&_nc_ht=scontent.fhan2-3.fna&oh=f53957d2f063d392295d6da43b53e0b4&oe=5FBB9ADA'                                            )
                                        ),
                                        Text("  Chia sẻ"),
                                      ],
                                    ),
                                  ],
                                ))
                          ],
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                      ),
                      // Center(
                      //   child: Text('Tất cả'),
                      // ),
                      color: Color.fromRGBO(201, 204, 209, 1),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
