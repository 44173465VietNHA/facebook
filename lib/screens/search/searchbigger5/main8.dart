import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyApp8 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TabBarDemo(),
    );
  }
}

class TabBarDemo extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<TabBarDemo>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _selectedIndex = 0;

  List<Widget> list = [
    Tab(
      child: Text("Tất cả"),
    ),
    Tab(
      child: Text("Bài viết"),
    ),
    Tab(
      child: Text("Mọi người"),
    ),
    Tab(
      child: Text("Ảnh"),
    ),
    Tab(
      child: Text("Video"),
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: list.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 1, // AddedAdded
        child: Scaffold(
          appBar: new AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              title: new Container(
                child: new TextField(
                  // obscuringCharacter: "Linh",
                  textAlign: TextAlign.left,
                  autofocus: false,
                  style: TextStyle(fontSize: 22.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    suffixIcon: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.close),
                        onPressed: () {}),
                    filled: true,
                    fillColor: Color.fromRGBO(240, 242, 245, 1),
                    hintText: 'Tìm kiếm',
                    contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(26.7),
                    ),
                  ),
                ),
                padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
              ),
              leading: Padding(
                  padding: EdgeInsets.only(left: 12),
                  child: IconButton(
                      color: Colors.black,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () {}))),
          body: new TabBarView(
            children: <Widget>[
              // SingleChildScrollView(
              Container(
                padding: EdgeInsets.fromLTRB(0, 2, 0, 0),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        child: Center(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
                            child: Column(
                              children: [
                                Image.network(
                                  'https://scontent.fhan2-5.fna.fbcdn.net/v/t1.15752-9/122425822_782301979226153_2444495438980758565_n.png?_nc_cat=107&ccb=2&_nc_sid=ae9488&_nc_ohc=pYcCmv3HejAAX_BM2pE&_nc_ht=scontent.fhan2-5.fna&oh=1a3a9dac80ded0895d617ad00fa7644b&oe=5FB83E86',
                                  width: 200,
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text('Không thể tải kết quả'),
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    //Center Column contents vertically,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(Icons.refresh),
                                      Text('Thử lại')
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(0),
                    color: Color.fromRGBO(199, 202, 207, 1),
                  ),
                ),
                color: Color.fromRGBO(201, 204, 209, 1),
              ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
