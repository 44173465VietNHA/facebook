import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/screens/search/listpost/ListPostSearch.dart';
import 'package:facebook/screens/search/listpost/SearchFuture.dart';
import 'package:flutter/material.dart';

class ListPostHome extends StatefulWidget {
  SearchResponse listPostResponse;
  ListPostHome({@required this.listPostResponse});
  _TabBarDemoState s;

  @override
  _TabBarDemoState createState() {
    this.s = new _TabBarDemoState(this.listPostResponse);
    return s;
  } 
}

class _TabBarDemoState extends State<ListPostHome> {

  SearchResponse listPostResponse;
  _TabBarDemoState(@required this.listPostResponse);

    @override
    Widget build(BuildContext context) {
      return Container(
        child:
        listPostResponse!=null?
          ListPostSearch(listPostResponse.data)
        :
        Center(
            child: CircularProgressIndicator(),
          ),
      );
    }
}
  