import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/screens/post/EmojiData.dart';
import 'package:facebook/screens/post/SingleEmoji.dart';
import 'package:facebook/screens/search/listpost/SinglePostSearch.dart';
import 'package:flutter/material.dart';

class ListEmoji extends StatelessWidget {
  List<String> data = EmojiData.data;
  List<String> image;

  ListEmoji() {
    print(data.length);
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: data.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 10,
        childAspectRatio: 3,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
      ),
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 0),
      itemBuilder: (BuildContext context, int index) {
        return SingleEmoji(data[index]);
      },
    );
  }
}
