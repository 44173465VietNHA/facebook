import 'dart:io';
import 'dart:math';

import 'package:facebook/api/friend/get_user_friends.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/screens/post/ListActivity.dart';
import 'package:facebook/screens/post/ListEmoji.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:flutter/services.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'dart:typed_data';
import 'package:file_picker/file_picker.dart';

class EmojiAndActivity extends StatefulWidget {
  @override
  _TabBarDemoState createState() => _TabBarDemoState();
}

class _TabBarDemoState extends State<EmojiAndActivity>
    with SingleTickerProviderStateMixin {

  TabController _controller;
  int _selectedIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = TabController(length: 2, vsync: this);
    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
      });
      print("Selected Index: " + _controller.index.toString());
    });
    // Create TabController for getting the index of current tab
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 2, // AddedAdded
        child: Scaffold(
            appBar: new AppBar(
                elevation: 0,
                backgroundColor: Colors.white,
                title: new Container(
                  child: new Text(
                    _controller.index==0?
                    'Bạn đang cảm thấy thế nào?'
                    :
                    'Bạn đang làm gì vậy?'
                    ,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  ),
                  padding: EdgeInsets.only(top: 0),
                ),
                leading: Padding(
                    padding: EdgeInsets.only(left: 12),
                    child: IconButton(
                        color: Colors.black,
                        icon: Icon(Icons.arrow_back),
                        onPressed: () {
                          Navigator.pop(context);
                        }))),
            backgroundColor: Colors.white,
            body: Scaffold(
              appBar: TabBar(
                controller: _controller,
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.blue,
                tabs: [
                  Container(
                    alignment: Alignment.center,
                    child: Tab(child: Text('Cảm xúc')),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Tab(child: Text('Hoạt động')),
                  ),
                ],
              ),
              backgroundColor: Colors.white,
              body: new TabBarView(
                controller: _controller,
                children: <Widget>[
                Container(
                  child: ListEmoji(),
                ),
                Container(
                  child: ListActivity(),
                )
              ]),
            )),
      ),
    );
  }
}
