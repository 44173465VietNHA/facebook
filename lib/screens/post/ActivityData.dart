class ActivityData {
  static var data = [
    'Đang chúc mừng...',
    'Đang xem...',
    'Đang ăn...',
    'Đang uống...',
    'Đang tham gia...',
    'Đang đi tới...',
    'Đang nghe...',
    'Đang tìm...',
    'Đang nghĩ về...',
    'Đang đọc...',
    'Đang chơi...',
    'Đang ủng hộ...',
  ];
}