import 'package:facebook/api/listpost/get_list_video_posts.dart';
import 'package:facebook/api/listpost/get_list_video_posts.dart';
import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/api/payload/SearchData.dart';
import 'package:facebook/api/payload/SearchResponse.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/video/VideoFuture.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class VideoTab extends StatefulWidget {
  _TabVideoTab s;
  @override
  _TabVideoTab createState() {
    s = _TabVideoTab();
    return s;
  }
}

class _TabVideoTab extends State<VideoTab> {
  Future<SearchResponse> searchResponse;
  String token;
  String authorization;
  String keywork;
  int user_id;
  int index;
  int count;
  TextEditingController searchField;
  VideoFuture searchFuture;
  Future<GetRequestFriendResponse> getFriendResponse;
  ScrollController _scrollViewController;

  SearchResponse listPostResponse;

  bool isLoading = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  var random;

  Future _loadData() async {
    // perform fetching data delay
    print("@@@@@@@@@@@@@@@@@@@@@@@@@: pullup");
    var sa = await get_list_video_post(
        token, token, user_id, index, count, Constants.lastVideoIdPost);
    setState(() {
      searchFuture.s.searchResponseS.data.addAll(sa.data);
      isLoading = false;
    });
    searchFuture.reload();
  }

  Future deleteData(SearchData data) {
    // perform fetching data delay
    print("@@@@@@@@@@@@@@@@@@@@@@@@@: deleteData");
    // List<SearchData> dataI = Constants.listPost.data;
    List<SearchData> dataI = searchFuture.s.searchResponseS.data;
    int i;
    SearchData deleteElement;
    for (i = 0; i < dataI.length; i++) {
      if (dataI[i].id == data.id) {
        deleteElement = dataI[i];
        print("#######################index: " + i.toString());
        break;
      }
    }
    setState(() {
      searchFuture.s.searchResponseS.data.removeAt(i);
    });
    searchFuture.reload();
  }

  Future refreshList() async {
    print("#######################: pulldown");
    refreshKey.currentState?.show(atTop: false);
    Constants.lastVideoIdPost = 100000;
    var sa = await get_list_video_post(
        token, token, user_id, index, count, Constants.lastVideoIdPost);
    setState(() {
      searchFuture.s.searchResponseS.data.addAll(sa.data);
    });
    searchFuture.reload();
    Constants.lastVideoIdPost = 100000;
    var sa1 = await get_list_video_post(
        token, token, user_id, index, count, Constants.lastVideoIdPost);
    while (searchFuture.s.searchResponseS.data.length > sa1.data.length) {
      setState(() {
        searchFuture.s.searchResponseS.data.removeAt(0);
      });
      searchFuture.reload();
    }
  }

  @override
  initState() {
    super.initState();
    authorization = Constants.authorization;
    token = Constants.token;
    user_id = Constants.user_id;
    searchResponse = get_list_video_post(
            token, token, user_id, index, count, Constants.lastVideoIdPost)
        as Future<SearchResponse>;
    _scrollViewController = new ScrollController();
    searchFuture = VideoFuture(
      searchResponse: searchResponse,
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: RefreshIndicator(
        key: refreshKey,
        child: Container(
          // child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                color: Colors.white,
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                          child: Text(
                            "Watch",
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          child: Text(""),
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 0.0),
                              child: Row(
                                children: [
                                  RawMaterialButton(
                                    onPressed: () {
                                      Navigator.pushNamed(context, SEARCH_POST);
                                    },
                                    elevation: 2.0,
                                    fillColor: Color.fromRGBO(229, 230, 235, 1),
                                    child: Icon(
                                      Icons.search,
                                      // size: 5.0,
                                    ),
                                    padding: EdgeInsets.all(4.0),
                                    shape: CircleBorder(),
                                  )
                                ],
                              )),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              color: Color.fromRGBO(229, 230, 235, 1),
                              textColor: Colors.red,
                              onPressed: () {},
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.live_tv_rounded,
                                    color: Colors.black,
                                    size: 20,
                                  ),
                                  Text(
                                    "Trực tiếp",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12.0,
                                    ),
                                  ),
                                ],
                              )),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              color: Color.fromRGBO(229, 230, 235, 1),
                              textColor: Colors.red,
                              onPressed: () {
                                // Navigator.pushNamed(context, ALL_FRIEND);
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    MdiIcons.foodForkDrink,
                                    color: Colors.black,
                                    size: 20,
                                  ),
                                  Text(
                                    "Ẩm thực",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12.0,
                                    ),
                                  ),
                                ],
                              )),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              color: Color.fromRGBO(229, 230, 235, 1),
                              textColor: Colors.red,
                              onPressed: () {
                                // Navigator.pushNamed(context, ALL_FRIEND);
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.gamepad_outlined,
                                    color: Colors.black,
                                    size: 20,
                                  ),
                                  Text(
                                    "Chơi game",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12.0,
                                    ),
                                  ),
                                ],
                              )),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (!isLoading &&
                          scrollInfo.metrics.pixels ==
                              scrollInfo.metrics.maxScrollExtent) {
                        _loadData();
                        // start loading data
                        setState(() {
                          isLoading = true;
                        });
                      }
                    },
                    child: searchFuture),
              ),
              Container(
                height: isLoading ? 50.0 : 0,
                color: Colors.transparent,
                child: Center(
                  child: new CircularProgressIndicator(),
                ),
              ),
            ],
          ),
          // ),
          color: Color.fromRGBO(201, 204, 209, 1),
        ),
        onRefresh: refreshList,
      ),
    );
  }
}
