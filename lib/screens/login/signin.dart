import 'dart:convert';

import 'package:facebook/api/login/login.dart';
import 'package:facebook/api/notification/notificationApi.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/login/register/signup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

import "package:http/http.dart" as http;

import 'AfterRegister/LoginFb.dart';

class signIn extends StatefulWidget {
  @override
  _signInState createState() => _signInState();
}

class _signInState extends State<signIn> {
  final _formKey = new GlobalKey<FormState>();
  String _phoneNumber;
  String _password;
  String _errorMessage;
  bool _isLoading;
  bool _isLoginForm;
  LoginResponse message;
  //kiem tra xem form da valid hay chua

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  //submit
  void validateAndSubmit() async {
    if (validateAndSave()) {
      try {
        // setState(() {
        //   _errorMessage = "";
        //   _isLoading = true;
        // });
        print(_phoneNumber);
        print(_password);
        message = await login(_phoneNumber, _password);
        setState(() {
          _isLoading = false;
        });
        // print("@@@@@@@@@@@@@@@@@@@@@@@@@: " + message.data.token);
        if (message.code == 1000) {
          Constants.token = message.data.token;
          Constants.avatar = message.data.avatar;
          Constants.userName = message.data.username;
          Constants.user_id = message.data.id;
          Constants.authorization = message.data.token;
          Navigator.popAndPushNamed(
              context,
              LOGIN_FB);
        }
      } catch (e) {
        print("Error : $e");
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    }
    (message != null && message.code != 1000)
        ? showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Thông báo"),
                content: Text("${message.message}"),
                actions: [
                  FlatButton(
                    child: Text("OK"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            })
        : Container(
            height: 0,
          );
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _isLoginForm = true;
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return new Scaffold(
        body: Stack(
      children: <Widget>[
        SignUpPage(context, size),
        _showCircularProgress(),
      ],
    ));
  }

  Widget SignUpPage(context, size) {
    return new SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _showImage(size),
          Container(
            padding: EdgeInsets.all(4),
            child: new Form(
              key: _formKey,
              child: new ListView(
                shrinkWrap: true,
                children: <Widget>[
                  _showEmailInput(),
                  _showPasswordInput(),
                  _showLoginButton(context),
                  _showForgetPassword(),
                  Divider(
                    height: 50,
                    indent: 30,
                    endIndent: 30,
                    thickness: 1,
                    color: Colors.grey,
                  ),
                  _showSignIn(context),
                  showErrorMessage(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _showImage(Size size) {
    return new Container(
        child: Image.asset(
      "assets/123.PNG",
      width: size.width,
      height: 250,
      fit: BoxFit.fill,
    ));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget _showEmailInput() {
    return new Container(
        padding: EdgeInsets.only(top: 6, left: 20, right: 20, bottom: 6),
        child: TextFormField(
          maxLines: 1,
          autofocus: false,
          decoration: InputDecoration(
              labelText: 'Số điện thoại hoặc email',
              labelStyle: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue))),
          validator: (value) => value.isEmpty ? 'Phone không thể trống' : null,
          onSaved: (value) => _phoneNumber = value.trim(),
        ));
  }

  Widget _showPasswordInput() {
    return new Container(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 12),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: InputDecoration(
            labelText: 'Mật khẩu', //label cua truong nhap du lieu
            labelStyle: TextStyle(
                //style cua label
                fontSize: 20,
                fontFamily: 'Montserrat', // kieu chu
                fontWeight: FontWeight.bold, // in dam
                color: Colors.grey), // mau sac
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.blue))),
        validator: (value) => value.isEmpty ? 'Mật khẩu không thể trống' : null,
        onSaved: (value) => _password = value
            .trim(), // nhap mat khau sau 1 khoang thoi gian thi no se che di
      ),
    );
  }

  Widget _showLoginButton(context) {
    return new GestureDetector(
      onTap: (){
        validateAndSubmit();
      },
      child: Container(
        padding: EdgeInsets.only(left: 20, top: 4, bottom: 4, right: 20),
        height: 55.0,
        child: Material(
          borderRadius: BorderRadius.circular(8.0),
          color: Colors.blue,
          elevation: 24.0,

            child: Center(
              child: Text(
                'Đăng nhập',
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'),
              ),
            ),

        ),
      ),
    );


  }

  Widget _showForgetPassword() {
    return new Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      child: Center(
        child: InkWell(
          child: Text(
            'Quên mật khẩu',
            style: TextStyle(
                fontSize: 18,
                color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontFamily: 'Montserrat',
                decoration: TextDecoration.underline),
          ),
        ),
      ),
    );
  }

  Widget _showSignIn(context) {
    return new GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => signUp()));
        },
        child: Container(
          padding: EdgeInsets.only(left: 60, top: 4, bottom: 10, right: 60),
          height: 55.0,
          child: Material(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.green,
            elevation: 24.0,
            child: Center(
              child: Text(
                'Tạo tài khoản Facebook mới',
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'),
              ),
            ),
          ),
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
// class MessageLogin{
//   int code;
//   String message;
//   DataReturn data;
//   MessageLogin({this.code,this.message, this.data});
//   factory MessageLogin.fromJson(Map<String, dynamic> json) {
//     return MessageLogin(
//         code: json['code'],
//         message: json['message'].toString(),
//         data: DataReturn.fromJson(json['data'])

//     );
//   }
// }
// class DataReturn{
//   int id;
//   String token;
//   String username;
//   String avatar;
//   String active;
//   DataReturn({this.id,this.username, this.token, this.active, this.avatar});
//   factory DataReturn.fromJson(Map<String, dynamic> json) {
//     print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//     print(json['id'] +"_"+ json['token'].toString() +"_"+ json['username'].toString());
//     return DataReturn(
//         id: json['id'],
//         token: json['token'].toString(),
//         username: json['username'].toString(),
//         avatar: json['avatar'].toString(),
//         active: json['active'].toString()
//     );
//   }
// }
