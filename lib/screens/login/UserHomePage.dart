import 'dart:math';

import 'package:facebook/api/friend/get_user_friends.dart';
import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/api/payload/GetUserInfoData.dart';
import 'package:facebook/api/payload/GetUserInfoResponse.dart';
import 'package:facebook/api/user/get_user_info.dart';
import 'package:facebook/router/Router.dart';
import 'package:facebook/screens/friend/friendtab/Friend.dart';
import 'package:facebook/screens/personalfuture/PersonalTab.dart';
import 'package:facebook/screens/personalfuture/ShowListFriends.dart';
import 'package:facebook/screens/post/AddPost.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class UserHomePage extends StatefulWidget {
  final int user_id;

  const UserHomePage({this.user_id});

  @override
  _UserHomePageState createState() => _UserHomePageState(user_id);
}

class _UserHomePageState extends State<UserHomePage> {
  final int user_id;
  GetUserInfoData user;
  _UserHomePageState(this.user_id);

  String token;
  String authorization;
  String _fullName = " Đoàn Diệu Anh";
  String _status = "Hoa oải hương";
  String _bio = "\"Em đã làm gì có người yêu? Em còn đang sợ ế đây này\"";
  String _followers = "173";
  String _posts = "24";
  String _scores = "450";
  int stateAccept = 1;
  int numberListFriends = 1000;
  Future<GetUserInfoResponse> getUserInfoResponse;
  Future<GetFriendResponse> getFriendResponse;

  @override
  initState() {
    super.initState();
    authorization = Constants.authorization;
    token = Constants.token;
    getUserInfoResponse = get_user_info(authorization, token, user_id)
        as Future<GetUserInfoResponse>;

    getFriendResponse = get_user_friends(authorization, token, user_id, 0, 0)
        as Future<GetFriendResponse>;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.white,
          leading: IconButton(
            padding: EdgeInsets.only(left: 5),
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 7),
                      child: InkWell(
                        onTap: () {},
                        child: Icon(
                          Icons.search,
                          size: 26,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Flexible(
                      child: TextField(
                        style: TextStyle(fontSize: 18.0, color: Colors.black),
                        decoration: InputDecoration(
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 4.0),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide:
                                  BorderSide(color: Colors.black45, width: 1),
                            ),
                            hintText: 'Tìm kiếm',
                            hintStyle:
                                TextStyle(color: Colors.black, fontSize: 18)),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        body: FutureBuilder<GetUserInfoResponse>(
          future: getUserInfoResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              user = snapshot.data.data;
              // return Container();
              return _UserPage(size, user);
            } else if (snapshot.hasError) {
              return Text("Loi");
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }

  Widget _UserPage(Size size, GetUserInfoData user) {
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _buildProfileImage(size),
                SizedBox(height: 1),
                _buildFullName(user.username),
                _showAddFriends(),
                _showInfomationUser(),
                Divider(
                  indent: 20,
                  endIndent: 20,
                  thickness: 0.5,
                  color: Colors.black,
                ),
                // _showListFriends(numberListFriends),
                ShowListFriends(user_id: user.id),
                Divider(
                  indent: 20,
                  endIndent: 20,
                  thickness: 4,
                  color: Colors.grey,
                ),
                _showContent(),
                PersonalTab(user_id: user.id),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildProfileImage(Size size) {
    return new Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/background.jpg"),
                  fit: BoxFit.cover)),
          child: Container(
            width: double.infinity,
            height: 240,
            child: Container(
                padding: EdgeInsets.only(left: size.width * 0.3),
                alignment: Alignment(0.0, 2.5),
                child: Row(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage:
                      user.avatar!=null?
                          NetworkImage(Constants.FileURI + user.avatar)
                          :AssetImage("assets/ramdom.jpg")
                          ,
                      radius: 80.0,
                    ),
                  ],
                )),
          ),
        ),
        Container(
            padding: EdgeInsets.only(left: size.width * 0.36, top: 20),
            child: Container(
              width: 50.0,
              height: 50.0,
              decoration: new BoxDecoration(
                color: Color.fromRGBO(210, 232, 235, 1),
                shape: BoxShape.circle,
              ),
              child: InkWell(
                onTap: () {},
                child: Icon(
                  Icons.camera_alt,
                  size: 28,
                ),
              ),
            ))
      ],
    );
  }

  Widget _buildFullName(_fullName) {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      _fullName,
      style: _nameTextStyle,
    );
  }

  Widget _showAddFriends() {
    Size size = MediaQuery.of(context).size;
    return new Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 10),
          height: 64,
          width: size.width * 0.8,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(color: Color.fromRGBO(23, 119, 242, 1))),
            color: Color.fromRGBO(23, 119, 242, 1),
            onPressed: () {
              // addFriendButton();
            },
            child: Container(
              padding: EdgeInsets.only(left: size.width * 0.15),
              child: Row(
                children: <Widget>[
                  if (Constants.user_id==user_id)
                  Icon(Icons.add_circle_outline, size: 24, color: Colors.white),
                  if (Constants.user_id==user_id)
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      "Thêm vào tin",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),


                  if (user.is_friend=="true")
                  Icon(Icons.message, size: 24, color: Colors.white),
                  if (user.is_friend=="true")
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      "Nhắn tin",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                  
                  if (Constants.user_id!=user_id&&user.is_friend=="false")
                    Icon(Icons.person_add, size: 24, color: Colors.white),
                  if (Constants.user_id!=user_id&&user.is_friend=="false")
                    Container(
                      padding: EdgeInsets.only(left: 5),
                      child: Text(
                        "Thêm bạn bè",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),

                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 7, left: 7),
          height: 60,
          width: size.width * 0.18,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.0),
                side: BorderSide(
                  color: Color.fromRGBO(229, 230, 235, 1),
                )),
            onPressed: () {
              Navigator.pushNamed(context, SETTING_PERSONAL);
            },
            color: Color.fromRGBO(229, 230, 235, 1),
            child: Icon(
              Icons.more_horiz,
              size: 26,
            ),
          ),
        ),
      ],
    );
  }

  Widget _showInfomationUser() {
    Size size = MediaQuery.of(context).size;
    return new Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 20, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.work,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Đã làm việc tại",
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "Học Viện Ngân Hàng",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 20, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.account_balance_outlined,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Đã học tại",
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "THPT Lê Quý Đôn",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 20, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.analytics_rounded,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      "Có",
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),
                  Container(
                    child: Text(
                      "47.019",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4),
                    child: Text(
                      "người theo dõi",
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 20, top: 15, bottom: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.more_horiz,
                    size: 24,
                    color: Colors.black45,
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 4, left: 15),
                    child: Text(
                      Constants.user_id == user_id
                          ? "Xem thêm thông tin của bạn"
                          : "Xem thêm thông tin của " + user.username,
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Constants.user_id == user_id
                ? Container(
                    padding: EdgeInsets.only(
                        left: 20, top: 10, bottom: 10, right: 20),
                    height: 64,
                    width: size.width,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      color: Color.fromRGBO(210, 230, 235, 1),
                      onPressed: () {
                        // addFriendButton();
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: size.width * 0.15),
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text(
                                "Chỉnh sửa chi tiết công khai",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Color.fromRGBO(23, 119, 242, 1)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ],
    );
  }

  Widget _showListFriends(int numberListFriends) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Text(
                "Bạn bè",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: size.width * 0.42),
              child: Text(
                " Tìm Bạn bè",
                style: TextStyle(fontSize: 22, color: Colors.blue),
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Text(
                "${numberListFriends} người bạn",
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.w400,
                    color: Colors.black45),
              ),
            )
          ],
        ),
        Container(
          padding: EdgeInsets.only(left: 10, right: 10.0),
          width: MediaQuery.of(context).size.width - 10.0,
          height: MediaQuery.of(context).size.height * 0.48,
          child: FutureBuilder<GetFriendResponse>(
            future: getFriendResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                child:
                GridView.count(
                  crossAxisCount: 3,
                  childAspectRatio: 0.7,
                  padding:
                      EdgeInsets.only(top: 4, left: 2, bottom: 4, right: 2),
                  mainAxisSpacing: 2,
                  crossAxisSpacing: 3,
                  children: <Widget>[
                    for (int i = 0; i < max(6, snapshot.data.data.total); i++)
                      _singleFriend("assets/ImagePost/7.jpg", "Diệu Anh Đoàn"),
                    // _singleFriend("assets/ImagePost/13.jpg", "Diệu Anh Trang"),
                    // _singleFriend("assets/ImagePost/9.jpg", "Diệu Anh Linh"),
                    // _singleFriend("assets/ImagePost/10.jpg", "Diệu Anh Vo"),
                    // _singleFriend("assets/ImagePost/11.jpg", "Diệu Anh Huong"),
                    // _singleFriend("assets/ImagePost/12.jpg", "Diệu Anh Giang"),
                  ],
                );
              } else if (snapshot.hasError) {
                return Text("Loi");
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ),
        Row(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 20),
              height: 64,
              width: size.width,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6.0),
                ),
                color: Color.fromRGBO(230, 235, 242, 1),
                onPressed: () {
                  // addFriendButton();
                },
                child: Container(
                  padding: EdgeInsets.only(left: size.width * 0.2),
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          "Xem tất cả bạn bè",
                          style: TextStyle(
                              fontSize: 19,
                              fontWeight: FontWeight.w500,
                              color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _singleFriend(String img, String name) {
    return new Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 100.0,
            width: 100.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                image:
                    DecorationImage(image: AssetImage(img), fit: BoxFit.fill)),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, left: 10, right: 10),
            child: Text(
              "${name}",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black),
            ),
          )
        ],
      ),
    );
  }

  Widget _showContent() {
    Size size = MediaQuery.of(context).size;
    return new Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Text(
                "Bài Viết",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                    color: Colors.black),
              ),
            ),
            Container(
              width: size.width * 0.32,
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10, right: 7, left: 7),
              height: 60,
              width: size.width * 0.18,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                    side: BorderSide(
                      color: Color.fromRGBO(229, 230, 235, 1),
                    )),
                onPressed: () {},
                color: Color.fromRGBO(229, 230, 235, 1),
                child: Icon(
                  Icons.add_road,
                  size: 26,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10, right: 7, left: 7),
              height: 60,
              width: size.width * 0.18,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.0),
                    side: BorderSide(
                      color: Color.fromRGBO(229, 230, 235, 1),
                    )),
                onPressed: () {},
                color: Color.fromRGBO(229, 230, 235, 1),
                child: Icon(
                  Icons.settings,
                  size: 26,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 5, 10, 0),
              child: CircleAvatar(
                  radius: 30,
                  backgroundImage:
                  user.avatar == null?AssetImage("assets/ramdom.jpg"):
                      NetworkImage(Constants.FileURI + user.avatar)),
            ),
            Flexible(
              child: TextField(
                // textInputAction:
                //     TextInputAction.newline,
                readOnly: true,
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => AddPost()));
                },
                style: TextStyle(fontSize: 15.0, color: Colors.black),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color.fromRGBO(240, 242, 245, 1),
                  hintText: 'Bạn đang nghĩ gì?',
                  hintStyle: TextStyle(color: Colors.black, fontSize: 18),
                  contentPadding: const EdgeInsets.only(left: 8.0, top: 11.0),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(26.7),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(26.7),
                  ),
                ),
              ),
            ),
          ],
        ),
        Divider(
          indent: 20,
          endIndent: 20,
          thickness: 1,
          color: Colors.grey,
        ),
        Container(
          height: 15,
        ),
        IntrinsicHeight(
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.video_call_outlined,
                      color: Colors.red,
                      size: 21,
                    ),
                    Container(
                      child: Text(
                        "Phát trực tiếp",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey),
                      ),
                    )
                  ],
                ),
              ),
              VerticalDivider(
                color: Colors.black,
                thickness: 1,
              ),
              Container(
                padding: EdgeInsets.only(left: 4, right: 4),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.image,
                      color: Colors.green,
                      size: 21,
                    ),
                    Container(
                      child: Text(
                        "Ảnh",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey),
                      ),
                    )
                  ],
                ),
              ),
              VerticalDivider(
                color: Colors.black,
                thickness: 1,
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.flag,
                      color: Colors.red,
                      size: 21,
                    ),
                    Container(
                      child: Text(
                        "Sự kiện trong đời",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 15,
        ),
        Divider(
          indent: 20,
          endIndent: 20,
          thickness: 5,
          color: Colors.grey,
        ),
        Container(
          height: 15,
        ),
        
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 20),
              child: Container(
                height: 50,
                width: 65,
                padding: EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Color.fromRGBO(229, 230, 235, 1)),
                child: Row(children: <Widget>[
                  Container(
                    child: Icon(
                      Icons.image,
                      size: 24,
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    child: Text(
                      "Ảnh",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Colors.black),
                    ),
                  )
                ]),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 10),
              child: Container(
                height: 50,
                width: 135,
                padding: EdgeInsets.only(left: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Color.fromRGBO(229, 230, 235, 1)),
                child: Row(children: <Widget>[
                  Container(
                    child: Icon(
                      Icons.star,
                      size: 24,
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    child: Text(
                      "Sự kiện trong đời",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Colors.black),
                    ),
                  )
                ]),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 10),
              child: Container(
                height: 50,
                width: 90,
                padding: EdgeInsets.only(left: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Color.fromRGBO(229, 230, 235, 1)),
                child: Row(children: <Widget>[
                  Container(
                    child: Icon(
                      Icons.music_note,
                      size: 24,
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    child: Text(
                      "Âm nhạc",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Colors.black),
                    ),
                  )
                ]),
              ),
            ),
          ],
        ),
        Container(
          height: 15,
        ),
        Divider(
          indent: 20,
          endIndent: 20,
          thickness: 5,
          color: Colors.grey,
        ),
      ],
    );
  }

  Widget _showUserListPost() {
    return new TabBarDemo();
  }
}
