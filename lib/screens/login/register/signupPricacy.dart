import 'dart:io';

import 'package:facebook/api/signup/first_signup.dart';
import 'package:facebook/screens/login/AfterRegister/FailRegister.dart';
import 'package:facebook/screens/login/AfterRegister/SavePassword.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import "package:http/http.dart" as http;
import 'dart:async';
import 'dart:convert';

class signUpPrivacy extends StatefulWidget {
  String phone;
  String pass;
  String firstName;
  String lastName;
  String date;
  String male;
  @override
  _signUpPrivacyState createState() => _signUpPrivacyState();
  signUpPrivacy(
      {Key key,
      this.phone,
      this.pass,
      this.firstName,
      this.lastName,
      this.date,
      this.male})
      : super(key: key);
}

class _signUpPrivacyState extends State<signUpPrivacy> {
  String _errorMessage;
  bool _isLoading;
  bool _isLoginForm;
  int codeReturn;
  ChangeInfoResponseToken message;
  //kiem tra xem form da valid hay chua

  //submit
  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    try {
      print(widget.date);
      print(widget.male);
      // final http.Response response = await http.post(
      //   Constants.Root + 'api/first_signup'
      //   ,
      //   headers: <String, String>{
      //     'Content-Type': 'application/json; charset=UTF-8',
      //   },
      //   body: jsonEncode(<String, String>{
      //     "phonenumber": widget.phone,
      //     "password": widget.pass,
      //     "first_name":widget.firstName,
      //     "last_name":widget.lastName,
      //     "birth_day":widget.date,
      //     "male":widget.male,
      //     "uuid": "1234123"
      //   }),
      // );
      message = await first_signup(widget.phone, widget.pass, widget.firstName,
          widget.lastName, widget.date, widget.male, "iphone");
      if (message.code == 1000) {
        Constants.token = message.token;
        Constants.avatar = message.data.avatar;
        Constants.userName = message.data.username;
        Constants.user_id = message.data.id;
        Constants.authorization = message.token;
        Constants.phone = widget.phone;
        Constants.password = widget.pass;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => SavePassword()));
      }
      setState(() {
        _isLoading = false;
      });
      print(message.code);
    } catch (e) {
      print("Error : $e");
      setState(() {
        _isLoading = false;
        _errorMessage = e.message;
        codeReturn = message.code;
      });
    }
    if (message.code != 1000) {
      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => FailRegister()));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Thông báo"),
            content: Text("${message.message}"),
            actions: [
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
    }
    
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _isLoginForm = true;
    super.initState();
  }

  void resetForm() {
    _errorMessage = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text(
          'Điều khoản và quyền riêng tư',
          style: TextStyle(
              fontSize: 22, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: SignUpPage(context),
    );
  }

  Widget SignUpPage(context) {
    return new SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _showText1(),
          _showText2(),
          _showText3(),
          _showButton(context),
          _showCircularProgress()
        ],
      ),
    );
  }

  Widget _showText1() {
    return new Container(
        padding: EdgeInsets.only(right: 20, left: 20, top: 40, bottom: 0),
        child: Center(
          child: Text(
            'Hoàn tất đăng ký',
            style: TextStyle(
                fontSize: 23, fontWeight: FontWeight.bold, color: Colors.black),
          ),
        ));
  }

  Widget _showText2() {
    return new Container(
      padding: EdgeInsets.only(right: 20, left: 20, top: 20),
      child: Text(
        'Bằng cách nhấn vào Đăng ký, bạn đồng ý vói Điều khoản, Chính sách dữ liệu và Chính sách cookie của chúng tôi.Bạn có thể nhận được thông'
        ' báo của chúng tôi qua SMS và chọn không nhận bất cứ lúc nào. Thông tin từ danh bạ của bạn sẽ được tải lên Facebook liên tục để chúng tôi có'
        ' thể gợi ý bạn bè, cung cấp và cải thiện quảng cáo cho bạn và người khác, cũng như mang đến dịch vụ tốt hơn.',
        style: TextStyle(fontSize: 17, color: Colors.black),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _showButton(context) {
    return new GestureDetector(
      onTap: () {
        validateAndSubmit();
      },
      child: Container(
        padding: EdgeInsets.only(right: 20, left: 20, top: 30, bottom: 0),
        height: 80.0,
        child: Material(
          borderRadius: BorderRadius.circular(8.0),
          color: Color.fromRGBO(24, 119, 242, 1),


            child: Center(
              child: Text(
                'Đăng ký',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat'),
              ),
            ),
          ),

      ),
    );

  }

  Widget _showText3() {
    return new Container(
      padding: EdgeInsets.only(right: 20, left: 20, top: 80),
      child: Text(
        'Thông tin liên hệ trong danh bạ của bạn, bao gồm tên, số điện thoại và biệt danh, sẽ được gửi tới facebook để Chúng tôi có thể gợi ý bạn bè, '
        ' cung cấp và cải thiện quảng cáo cho bạn và người khác, cũng như mang đến dịch vụ tốt hơn. Bạn có thể tắt tính năng này trong phần Cài đặt, quản lý hoặc xóa bỏ thông tin liên hệ với mình đã chia sẻ cho Facebook. Tìm hiểu thêm',
        style: TextStyle(fontSize: 15, color: Colors.black),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
}

class MessageLogin {
  int code;
  String message;
  MessageLogin({this.code, this.message});
  factory MessageLogin.fromJson(Map<String, dynamic> json) {
    return MessageLogin(
      code: json['code'],
      message: json['message'].toString(),
    );
  }
}
