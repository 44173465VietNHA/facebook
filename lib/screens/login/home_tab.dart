import 'package:facebook/router/Router.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:facebook/common/constants.dart' as Constants;

class MenuTab extends StatefulWidget {
  @override
  State createState() => MenuTabState();
}

class MenuTabState extends State<MenuTab> {
  bool isExpand = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            color: const Color(0xfff1f2f6),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 15.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 8,
                        child: Text('Menu',
                            style: TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.bold)),
                      ),
                      const Spacer(),
                      Expanded(
                          flex: 1,
                          child: RawMaterialButton(
                            onPressed: () {
                              Navigator.pushNamed(context, SEARCH_POST);
                            },
                            fillColor: Colors.white,
                            child: Icon(
                              Icons.search,
                              size: 25.0,
                            ),
                            padding: EdgeInsets.all(2.0),
                            shape: CircleBorder(),
                          )),
                      const Spacer(),
                    ],
                  ),
                ),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 15.0),
                        CircleAvatar(
                          radius: 25.0,
                          backgroundImage: Constants.avatar == null
                              ? AssetImage("assets/ramdom.jpg")
                              : NetworkImage(
                                  Constants.FileURI + Constants.avatar),
                        ),
                        SizedBox(width: 20.0),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, PERSONAL_PAGE,
                                arguments: {"id": Constants.user_id});
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(Constants.userName,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0)),
                              SizedBox(height: 5.0),
                              Text(
                                'Xem trang cá nhân của bạn',
                                style: TextStyle(color: Colors.grey),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Divider(
                    color: Colors.black26,
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 2 - 20,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/banbe.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Bạn bè',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/video.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Video trên Watch',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/kiniem.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Kỉ niệm',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/mesnhi.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Messenger nhí',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/shop.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Marketplace',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/job.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Việc làm',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/ungpho.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Ứng phó khẩn cấp',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/fbpay.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Facebook Pay',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 2 - 20,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/daluu.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Đã lưu',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/event.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Sự kiện',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/nhom.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Nhóm',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/choigame.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Chơi game',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/trang.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Trang',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/gandaynhat.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Gần đây nhất',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/uudai.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Ưu đãi',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(
                                left: 0,
                                top: 4.0,
                                bottom: 4.0,
                                right: 0,
                              ),
                              color: Colors.transparent,
                              child: Material(
                                color: Colors.white,
                                elevation: 5.0,
                                borderRadius: BorderRadius.circular(10.0),
                                shadowColor: Colors.black,
                                child: InkWell(
                                  customBorder: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  onTap: () {},
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15.0),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(height: 15.0),
                                          new Image(
                                            image: new AssetImage(
                                                "assets/image/date.png"),
                                            height: 24,
                                            width: 24,
                                            color: null,
                                            fit: BoxFit.scaleDown,
                                            alignment: Alignment.center,
                                          ),
                                          Text('Hẹn hò',
                                              style: TextStyle(fontSize: 16.0)),
                                          SizedBox(height: 10.0),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: ExpansionTile(
                      leading: new Image(
                        image: new AssetImage("assets/image/xemthem.png"),
                        height: 30,
                        width: 30,
                        color: null,
                        fit: BoxFit.scaleDown,
                        alignment: Alignment.center,
                      ),
                      title: Container(
                        child: isExpand
                            ? Text(
                                "Ẩn bớt",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                ),
                              )
                            : Text(
                                "Xem thêm",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                ),
                              ),
                      ),
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/avatar.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Avatar',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.help, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/chiendich.png"),
                                    height: 20,
                                    width: 20,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Chiến dịch gây quỹ',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/fbpay.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Facebook Pay',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/gandaynhat.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Gần đây nhất',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/mesnhi.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Messenger nhí',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/thoitiet.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Thời tiết',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/ungpho.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Ứng phó khẩn cấp',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/uudai.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Ưu đãi',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/live.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Video trực tiếp',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                        ),
                      ],
                      onExpansionChanged: (bool expanding) =>
                          setState(() => this.isExpand = expanding),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Color(0xffe6e7e9),
                    border: Border(
                      top: BorderSide(width: 0.5, color: Color(0xffcbccd0)),
                    ),
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: ExpansionTile(
                      leading: new Image(
                        image: new AssetImage("assets/image/help.png"),
                        height: 30,
                        width: 30,
                        color: null,
                        fit: BoxFit.scaleDown,
                        alignment: Alignment.center,
                      ),
                      title: Container(
                        child: Text(
                          "Trợ giúp & hỗ trợ",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/help_center.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Trung tâm trợ giúp',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.help, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/mail.png"),
                                    height: 20,
                                    width: 20,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Hộp thư hỗ trợ',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/chat.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Cộng đồng trợ giúp',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/warn.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Báo cáo sự cố',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/book.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Điều khoản & chính sách',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Color(0xffe6e7e9),
                    border: Border(
                      top: BorderSide(width: 0.5, color: Color(0xffcbccd0)),
                    ),
                  ),
                  child: Material(
                    color: Colors.transparent,
                    child: ExpansionTile(
                      leading: new Image(
                        image: new AssetImage("assets/image/caidat.png"),
                        height: 30,
                        width: 30,
                        color: null,
                        fit: BoxFit.scaleDown,
                        alignment: Alignment.center,
                      ),
                      title: Container(
                        child: Text(
                          "Cài đặt & quyền riêng tư",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.account_circle_sharp, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/setting.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Cài đặt',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  // Icon(Icons.help, size: 24.0, color: Colors.grey[700]),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/short.png"),
                                    height: 20,
                                    width: 20,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Lối tắt quyền riêng tư',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/clock.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Thời gian bạn trên Facebook',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/request.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Yêu cầu từ thiết bị',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/recent.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Hoạt động quảng cáo gần đây',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/wifi.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Tìm Wi-Fi',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/night.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Chế độ tối',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image: new AssetImage(
                                        "assets/image/languages.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Ngôn ngữ',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/data.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Trình tiết kiệm dữ liệu',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 15.0,
                            top: 4.0,
                            right: 15.0,
                            bottom: 4.0,
                          ),
                          color: Colors.transparent,
                          height: 60.0,
                          child: Material(
                            color: Colors.white,
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: Colors.black,
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onTap: () {},
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                  ),
                                  new Image(
                                    image:
                                        new AssetImage("assets/image/key.png"),
                                    height: 24,
                                    width: 24,
                                    color: null,
                                    fit: BoxFit.scaleDown,
                                    alignment: Alignment.center,
                                  ),
                                  SizedBox(width: 10.0),
                                  Text('Trình tạo mã',
                                      style: TextStyle(fontSize: 16.0)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 65.0,
                  // padding: EdgeInsets.symmetric(horizontal: 15.0),
                  decoration: BoxDecoration(
                    color: Color(0xffe6e7e9),
                    border: Border(
                      top: BorderSide(width: 0.5, color: Color(0xffcbccd0)),
                    ),
                  ),
                  child: Material(
                    color: Color(0xffe6e7e9),
                    child: InkWell(
                      onTap: () {
                        Navigator.pushNamedAndRemoveUntil(
                            context, SIGN_IN, (Route<dynamic> route) => false);
                      },
                      child: ListTile(
                        title: Text(
                          'Đăng xuất',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17.0,
                          ),
                        ),
                        leading: new Image(
                          image: new AssetImage("assets/image/log_out.png"),
                          height: 30,
                          width: 30,
                          color: null,
                          fit: BoxFit.scaleDown,
                          alignment: Alignment.center,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
