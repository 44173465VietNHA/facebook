import 'package:facebook/router/Router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  int userId;
  int friendId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Chat select"),
          automaticallyImplyLeading: false,
        ),
        body: Center(
            child: Column(children: <Widget>[
          TextField(
            decoration: new InputDecoration(labelText: "Enter userId"),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            maxLength: 20,
            onChanged: (String value) {
              this.userId = int.parse(value);
            },
          ),
          TextField(
            decoration: new InputDecoration(labelText: "Enter friendId"),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            maxLength: 20,
            onChanged: (String value) {
              this.friendId = int.parse(value);
            },
            onSubmitted: (String value) {
              final int userId = this.userId;
              final int friendId = int.parse(value);
              if (userId != friendId)
                Navigator.pushNamed(context, CHAT_FRIEND, arguments: {
                  "userId": userId,
                  "friendId": friendId
                });
            },
          ),
        ])));
  }
}

class Message {
  final int fromUserId;
  final String content;

  Message(this.fromUserId, this.content);
}

class ChatFriend extends StatefulWidget {
  final int userId;
  final int friendId;

  const ChatFriend({Key key, this.userId, this.friendId}) : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<ChatFriend> {
  final TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // log("userId" + widget.userId.toString());
    // log("fid" + widget.friendId.toString());
    return Scaffold(
        appBar: AppBar(
          title: Text("Chat"),
          automaticallyImplyLeading: true,
        ),
        body: Column(children: <Widget>[
          // List of messages
          buildListMessage(),

          // Input content
          buildInput(),
        ]));
  }

  Widget buildListMessage() {
    final FirebaseFirestore firestore = FirebaseFirestore.instance;
    final String boxId = (widget.userId < widget.friendId)
        ? (widget.userId.toString() + "_" + widget.friendId.toString())
        : (widget.friendId.toString() + "_" + widget.userId.toString());
    return StreamBuilder(
        stream: firestore
            .collection("boxes")
            .doc(boxId)
            .collection("messages")
            .orderBy("createTime", descending: true)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData || snapshot.data == null)
            return CircularProgressIndicator();
          List<QueryDocumentSnapshot> messages = snapshot.data.docs;
          return Flexible(
              child: ListView.builder(
                  padding: EdgeInsets.all(10.0),
                  itemBuilder: (context, index) => buildItem(
                      index,
                      Message(messages[index].get("fromUserId"),
                          messages[index].get("content"))),
                  itemCount: messages.length,
                  reverse: true,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true));
        });
  }

  Widget buildInput() {
    return Container(
      child: Row(children: <Widget>[
        Flexible(
          child: TextField(
            textInputAction: TextInputAction.newline,
            style: TextStyle(fontSize: 15.0, color: Colors.black),
            controller: textEditingController,
            decoration: InputDecoration.collapsed(
                hintText: 'Type your message...',
                hintStyle: TextStyle(color: Colors.black)),
          ),
        ),
        Material(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: IconButton(
              icon: Icon(Icons.send),
              onPressed: () => onSendMessage(textEditingController.text),
            ),
          ),
        ),
      ]),
      width: double.infinity,
      height: 30.0,
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.grey, width: 0.5)),
          color: Colors.white),
    );
  }

  Widget buildItem(int index, message) {
    if (message.fromUserId == widget.userId) {
      // Right (my message)
      return Row(children: <Widget>[
        Text(
          message.content,
          style: TextStyle(color: Colors.blue),
        ),
      ], mainAxisAlignment: MainAxisAlignment.end);
    } else {
      // Left (peer message)
      return Row(children: <Widget>[
        Text(
          message.content,
          style: TextStyle(color: Colors.green),
        ),
      ], crossAxisAlignment: CrossAxisAlignment.start);
    }
  }

  void onSendMessage(
    String content,
  ) {
    if (content.trim() != '') {
      textEditingController.clear();

      final FirebaseFirestore firestore = FirebaseFirestore.instance;
      final String boxId = (widget.userId < widget.friendId)
          ? (widget.userId.toString() + "_" + widget.friendId.toString())
          : (widget.friendId.toString() + "_" + widget.userId.toString());

      firestore.collection("boxes").doc(boxId).collection("messages").add({
        "createTime": FieldValue.serverTimestamp(),
        "fromUserId": widget.userId,
        "content": content
      });
    }
  }
}
