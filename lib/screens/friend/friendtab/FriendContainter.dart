import 'package:facebook/api/payload/GetRequestFriendResponse.dart';
import 'package:facebook/screens/friend/friendtab/LoiMoiFuture.dart';
import 'package:flutter/material.dart';

class FriendContainer extends StatelessWidget {
  final Future<GetRequestFriendResponse> getFriendResponse;
  FriendContainer({@required this.getFriendResponse});

  @override
  Widget build(BuildContext context) { 
                return  Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 2),
                    color: Color.fromRGBO(201, 204, 209, 1),
                    child: LoiMoiFuture(loiMoiResponse: getFriendResponse),
                          // // Gợi ý bạn bè (tạm thời bỏ qua)
                          // Container(
                          //   padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                          //   color: Color.fromRGBO(201, 204, 209, 1),
                          //   child: Container(
                          //     padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          //     color: Colors.white,
                          //     child: Column(
                          //       children: [
                          //         Row(
                          //           crossAxisAlignment: CrossAxisAlignment.start,
                          //           mainAxisAlignment: MainAxisAlignment.start,
                          //           children: [
                          //             Container(
                          //               padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                          //               child: Text(
                          //                 "Những người bạn có thể biết",
                          //                 style: TextStyle(
                          //                     fontSize: 20,
                          //                     fontWeight: FontWeight.bold),
                          //               ),
                          //             ),
                          //             Expanded(
                          //                 child: Container(
                          //                   padding: EdgeInsets.only(top: 3),
                          //                   child: Text(
                          //                     "",
                          //                     style: TextStyle(
                          //                       fontSize: 22,
                          //                       fontWeight: FontWeight.bold,
                          //                       color: Color.fromRGBO(220, 47, 74, 1),
                          //                     ),
                          //                   ),
                          //                 )),
                          //             Align(
                          //               alignment: Alignment.topRight,
                          //               child: Container(
                          //                   padding: EdgeInsets.fromLTRB(0, 5, 10, 0),
                          //                   child: Text(
                          //                     "",
                          //                     style: TextStyle(
                          //                       color: Colors.blue,
                          //                       fontSize: 20,
                          //                       // fontWeight: FontWeight.bold,
                          //                     ),
                          //                   )),
                          //             ),
                          //           ],
                          //         ),
                          //         Column(
                          //           crossAxisAlignment: CrossAxisAlignment.start,
                          //           mainAxisAlignment: MainAxisAlignment.start,
                          //           children: [
                          //             Container(
                          //               padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                 MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   CircleAvatar(
                          //                       radius: 50,
                          //                       backgroundImage: NetworkImage(
                          //                           'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/90791271_3107297682665702_3597589822606147584_o.png?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=0WyIlDZi5DMAX8-FiNK&_nc_ht=scontent.fhan2-2.fna&oh=29d4ef23f205eae5858d09f467fe7d6c&oe=5FBA575C')),
                          //                   Expanded(
                          //                     child: Column(
                          //                       mainAxisAlignment:
                          //                       MainAxisAlignment.spaceBetween,
                          //                       children: [
                          //                         IntrinsicHeight(
                          //                           child: Row(
                          //                             mainAxisAlignment:
                          //                             MainAxisAlignment
                          //                                 .spaceBetween,
                          //                             children: [
                          //                               Container(
                          //                                 child: Text(
                          //                                   "Trung Nguyen Duc",
                          //                                   style: TextStyle(
                          //                                     fontSize: 16,
                          //                                     fontWeight:
                          //                                     FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                               Container(
                          //                                 child: Text(
                          //                                   "",
                          //                                   style: TextStyle(
                          //                                     // fontSize: 16,
                          //                                     // fontWeight: FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                             ],
                          //                           ),
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.fromLTRB(
                          //                               10, 0, 10, 10),
                          //                           child: Row(
                          //                               crossAxisAlignment:
                          //                               CrossAxisAlignment.start,
                          //                               mainAxisAlignment:
                          //                               MainAxisAlignment.start,
                          //                               children: [
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 0),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         24, 120, 243, 1),
                          //                                     textColor: Colors.red,
                          //                                     // padding: EdgeInsets.all(14.0),
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Thêm bạn bè",
                          //                                       style: TextStyle(
                          //                                         color: Colors.white,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 10),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         229, 230, 235, 1),
                          //                                     textColor: Colors.red,
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Gỡ",
                          //                                       style: TextStyle(
                          //                                         color: Colors.black,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                               ]),
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //             Container(
                          //               padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                 MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   CircleAvatar(
                          //                       radius: 50,
                          //                       backgroundImage: NetworkImage(
                          //                           'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/90791271_3107297682665702_3597589822606147584_o.png?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=0WyIlDZi5DMAX8-FiNK&_nc_ht=scontent.fhan2-2.fna&oh=29d4ef23f205eae5858d09f467fe7d6c&oe=5FBA575C')),
                          //                   Expanded(
                          //                     child: Column(
                          //                       mainAxisAlignment:
                          //                       MainAxisAlignment.spaceBetween,
                          //                       children: [
                          //                         IntrinsicHeight(
                          //                           child: Row(
                          //                             mainAxisAlignment:
                          //                             MainAxisAlignment
                          //                                 .spaceBetween,
                          //                             children: [
                          //                               Container(
                          //                                 child: Text(
                          //                                   "Trung Nguyen Duc",
                          //                                   style: TextStyle(
                          //                                     fontSize: 16,
                          //                                     fontWeight:
                          //                                     FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                               Container(
                          //                                 child: Text(
                          //                                   "",
                          //                                   style: TextStyle(
                          //                                     // fontSize: 16,
                          //                                     // fontWeight: FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                             ],
                          //                           ),
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.fromLTRB(
                          //                               10, 0, 10, 10),
                          //                             child: Text(
                          //                               'Đã hủy yêu cầu'
                          //                               ),
                          //                           alignment: Alignment.topLeft,
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.fromLTRB(
                          //                               10, 0, 10, 10),
                          //                           child: Row(
                          //                               crossAxisAlignment:
                          //                               CrossAxisAlignment.start,
                          //                               mainAxisAlignment:
                          //                               MainAxisAlignment.start,
                          //                               children: [
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 0),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         24, 120, 243, 1),
                          //                                     textColor: Colors.red,
                          //                                     // padding: EdgeInsets.all(14.0),
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Thêm bạn bè",
                          //                                       style: TextStyle(
                          //                                         color: Colors.white,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 10),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         229, 230, 235, 1),
                          //                                     textColor: Colors.red,
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Gỡ",
                          //                                       style: TextStyle(
                          //                                         color: Colors.black,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                               ]),
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //             Container(
                          //               padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                 MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   CircleAvatar(
                          //                       radius: 50,
                          //                       backgroundImage: NetworkImage(
                          //                           'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/90791271_3107297682665702_3597589822606147584_o.png?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=0WyIlDZi5DMAX8-FiNK&_nc_ht=scontent.fhan2-2.fna&oh=29d4ef23f205eae5858d09f467fe7d6c&oe=5FBA575C')),
                          //                   Expanded(
                          //                     child: Column(
                          //                       mainAxisAlignment:
                          //                       MainAxisAlignment.spaceBetween,
                          //                       children: [
                          //                         IntrinsicHeight(
                          //                           child: Row(
                          //                             mainAxisAlignment:
                          //                             MainAxisAlignment
                          //                                 .spaceBetween,
                          //                             children: [
                          //                               Container(
                          //                                 child: Text(
                          //                                   "Trung Nguyen Duc",
                          //                                   style: TextStyle(
                          //                                     fontSize: 16,
                          //                                     fontWeight:
                          //                                     FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                               Container(
                          //                                 child: Text(
                          //                                   "",
                          //                                   style: TextStyle(
                          //                                     // fontSize: 16,
                          //                                     // fontWeight: FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                             ],
                          //                           ),
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.only(left: 10),
                          //                           child: Text('Đã gửi yêu cầu'),
                          //                           alignment: Alignment.topLeft,
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.fromLTRB(
                          //                               10, 0, 10, 10),
                          //                           child: Row(
                          //                               crossAxisAlignment:
                          //                               CrossAxisAlignment.start,
                          //                               mainAxisAlignment:
                          //                               MainAxisAlignment.start,
                          //                               children: [
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 0),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         229, 230, 235, 1),
                          //                                     textColor: Colors.red,
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         95, 0, 95, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Hủy",
                          //                                       style: TextStyle(
                          //                                         color: Colors.black,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                               ]),
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //             Container(
                          //               padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                 MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   CircleAvatar(
                          //                       radius: 50,
                          //                       backgroundImage: NetworkImage(
                          //                           'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/90791271_3107297682665702_3597589822606147584_o.png?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=0WyIlDZi5DMAX8-FiNK&_nc_ht=scontent.fhan2-2.fna&oh=29d4ef23f205eae5858d09f467fe7d6c&oe=5FBA575C')),
                          //                   Expanded(
                          //                     child: Column(
                          //                       mainAxisAlignment:
                          //                       MainAxisAlignment.spaceBetween,
                          //                       children: [
                          //                         IntrinsicHeight(
                          //                           child: Row(
                          //                             mainAxisAlignment:
                          //                             MainAxisAlignment
                          //                                 .spaceBetween,
                          //                             children: [
                          //                               Container(
                          //                                 child: Text(
                          //                                   "Trung Nguyen Duc",
                          //                                   style: TextStyle(
                          //                                     fontSize: 16,
                          //                                     fontWeight:
                          //                                     FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                               Container(
                          //                                 child: Text(
                          //                                   "",
                          //                                   style: TextStyle(
                          //                                     // fontSize: 16,
                          //                                     // fontWeight: FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                             ],
                          //                           ),
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.fromLTRB(
                          //                               10, 0, 10, 10),
                          //                           child: Row(
                          //                               crossAxisAlignment:
                          //                               CrossAxisAlignment.start,
                          //                               mainAxisAlignment:
                          //                               MainAxisAlignment.start,
                          //                               children: [
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 0),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         24, 120, 243, 1),
                          //                                     textColor: Colors.red,
                          //                                     // padding: EdgeInsets.all(14.0),
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Thêm bạn bè",
                          //                                       style: TextStyle(
                          //                                         color: Colors.white,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 10),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         229, 230, 235, 1),
                          //                                     textColor: Colors.red,
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Gỡ",
                          //                                       style: TextStyle(
                          //                                         color: Colors.black,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                               ]),
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //             Container(
                          //               padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          //               child: Row(
                          //                 mainAxisAlignment:
                          //                 MainAxisAlignment.spaceBetween,
                          //                 children: [
                          //                   CircleAvatar(
                          //                       radius: 50,
                          //                       backgroundImage: NetworkImage(
                          //                           'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/90791271_3107297682665702_3597589822606147584_o.png?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=0WyIlDZi5DMAX8-FiNK&_nc_ht=scontent.fhan2-2.fna&oh=29d4ef23f205eae5858d09f467fe7d6c&oe=5FBA575C')),
                          //                   Expanded(
                          //                     child: Column(
                          //                       mainAxisAlignment:
                          //                       MainAxisAlignment.spaceBetween,
                          //                       children: [
                          //                         IntrinsicHeight(
                          //                           child: Row(
                          //                             mainAxisAlignment:
                          //                             MainAxisAlignment
                          //                                 .spaceBetween,
                          //                             children: [
                          //                               Container(
                          //                                 child: Text(
                          //                                   "Trung Nguyen Duc",
                          //                                   style: TextStyle(
                          //                                     fontSize: 16,
                          //                                     fontWeight:
                          //                                     FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                               Container(
                          //                                 child: Text(
                          //                                   "",
                          //                                   style: TextStyle(
                          //                                     // fontSize: 16,
                          //                                     // fontWeight: FontWeight.bold,
                          //                                   ),
                          //                                 ),
                          //                                 padding: EdgeInsets.fromLTRB(
                          //                                     10, 10, 0, 0),
                          //                               ),
                          //                             ],
                          //                           ),
                          //                         ),
                          //                         Container(
                          //                           padding: EdgeInsets.fromLTRB(
                          //                               10, 0, 10, 10),
                          //                           child: Row(
                          //                               crossAxisAlignment:
                          //                               CrossAxisAlignment.start,
                          //                               mainAxisAlignment:
                          //                               MainAxisAlignment.start,
                          //                               children: [
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 0),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         24, 120, 243, 1),
                          //                                     textColor: Colors.red,
                          //                                     // padding: EdgeInsets.all(14.0),
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Thêm bạn bè",
                          //                                       style: TextStyle(
                          //                                         color: Colors.white,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                                 Container(
                          //                                   padding: EdgeInsets.only(
                          //                                       left: 10),
                          //                                   child: FlatButton(
                          //                                     shape:
                          //                                     RoundedRectangleBorder(
                          //                                       borderRadius:
                          //                                       BorderRadius
                          //                                           .circular(8.0),
                          //                                     ),
                          //                                     color: Color.fromRGBO(
                          //                                         229, 230, 235, 1),
                          //                                     textColor: Colors.red,
                          //                                     padding:
                          //                                     EdgeInsets.fromLTRB(
                          //                                         11, 0, 11, 0),
                          //                                     onPressed: () {},
                          //                                     child: Text(
                          //                                       "Gỡ",
                          //                                       style: TextStyle(
                          //                                         color: Colors.black,
                          //                                         fontSize: 16.0,
                          //                                       ),
                          //                                     ),
                          //                                   ),
                          //                                 ),
                          //                               ]),
                          //                         ),
                          //                       ],
                          //                     ),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //           ],
                          //         ),
                          //       ],
                          //     ),
                          //   ),
                          // ),  
                );
  }
}
