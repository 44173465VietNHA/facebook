import 'package:facebook/api/block/set_block.dart';
import 'package:facebook/api/payload/FriendsData.dart';
import 'package:facebook/router/Router.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;

class SingleAllFriend extends StatefulWidget {
  final FriendsData data;
  SingleAllFriend(@required this.data);
  @override
  _MyListState createState() => _MyListState(data);
}



class _MyListState extends State<SingleAllFriend> {
  final FriendsData data;
  _MyListState(@required this.data);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, PERSONAL_PAGE,
                                          arguments: {
                                            "id": data.id
                                          });
            },
            child: CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage(Constants.FileURI + data.avatar)),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IntrinsicHeight(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          data.username,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                      ),
                      Sheet(data),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Sheet extends StatelessWidget {
  final FriendsData data;
  const Sheet(@required this.data);
  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
        color: Colors.white,
        padding: EdgeInsets.all(0),
        elevation: 0,
        child: Container(
          color: Colors.white,
          child: Icon(Icons.more_horiz),
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
        ),
        onPressed: () {
          // Show sheet here
          showModalBottomSheet<void>(
              context: context,
              builder: (BuildContext context) {
                return new Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.white, spreadRadius: 7),
                    ],
                  ),
                  child: new Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            // borderRadius: BorderRadius.circular(10),
                            border: Border(
                                bottom: BorderSide(
                              color: Color.fromRGBO(201, 204, 209, 1),
                            )),
                          ),
                          child: Row(
                            children: [
                              Container(
                                child: CircleAvatar(
                                  radius: 25,
                                  backgroundImage: NetworkImage(
                                    Constants.FileURI + data.avatar,
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      data.username,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                      ),
                                    ),
                                    Text(
                                      'Là bạn bè từ tháng 1 năm 2020',
                                      style: TextStyle(
                                          // fontWeight: FontWeight.bold,
                                          ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () => messenger(data.id),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Row(
                              children: [
                                Image.network(
                                  'https://scontent.fhan2-6.fna.fbcdn.net/v/t1.15752-9/cp0/122441465_353121899242262_8300615376262953240_n.png?_nc_cat=103&ccb=2&_nc_sid=ae9488&_nc_ohc=o7DAXOaJm3sAX9OT6Cz&_nc_ht=scontent.fhan2-6.fna&oh=821206223f968830e4d83146345d0135&oe=5FE802A3',
                                  width: 32,
                                  height: 32,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    'Nhắn tin cho ' + data.username,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Container(
                        //   padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                        //   child: Row(
                        //     children: [
                        //       Image.network(
                        //         "https://scontent.fhan2-6.fna.fbcdn.net/v/t1.15752-9/cp0/122441465_353121899242262_8300615376262953240_n.png?_nc_cat=103&ccb=2&_nc_sid=ae9488&_nc_ohc=o7DAXOaJm3sAX9OT6Cz&_nc_ht=scontent.fhan2-6.fna&oh=821206223f968830e4d83146345d0135&oe=5FE802A3",
                        //         width: 32,
                        //         height: 32,
                        //       ),
                        //       Container(
                        //         padding: EdgeInsets.only(left: 10),
                        //         child: Column(
                        //           crossAxisAlignment: CrossAxisAlignment.start,
                        //           mainAxisAlignment: MainAxisAlignment.start,
                        //           children: [
                        //             Text(
                        //               'Bỏ theo dõi Trang',
                        //               style: TextStyle(
                        //                 fontWeight: FontWeight.bold,
                        //               ),
                        //             ),
                        //             Container(
                        //               width: 280,
                        //               child: Text(
                        //                   'Không nhìn thấy bài viết của nhau nữa nhưng vẫn là bạn bè'),
                        //             )
                        //           ],
                        //         ),
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        InkWell(
                          onTap: () => blockUser(data.id),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Row(
                              children: [
                                Image.network(
                                  "https://scontent.fhan2-5.fna.fbcdn.net/v/t1.15752-9/cp0/122547477_347150819713233_3967975716929746308_n.png?_nc_cat=109&ccb=2&_nc_sid=ae9488&_nc_ohc=4Ap_pqMX9-QAX8Gn5Ah&_nc_ht=scontent.fhan2-5.fna&oh=1b4a571df8fec581365a128eb6bfa819&oe=5FE784F6",
                                  width: 32,
                                  height: 32,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Chặn ' + data.username,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Container(
                                        width: 280,
                                        child: Text(
                                            'Trang sẽ không thể nhìn thấy bạn hoặc liên hệ với bạn trên Facebook'),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () => cancelFriend(data.id),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                          child: Row(
                            children: [
                              Image.network(
                                'https://scontent.fhan2-6.fna.fbcdn.net/v/t1.15752-9/cp0/122533895_357369105696312_8060120002792979009_n.png?_nc_cat=100&ccb=2&_nc_sid=ae9488&_nc_ohc=kqPTajQPGFUAX9lffyk&_nc_ht=scontent.fhan2-6.fna&oh=af104555154a36c824522407fcb34a5c&oe=5FE6FE0A',
                                width: 32,
                                height: 32,
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Hủy kết bạn với ' + data.username,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red,
                                      ),
                                    ),
                                    Container(
                                      width: 280,
                                      child: Text(
                                          'Xóa Trang khỏi danh sách bạn bè'),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        ),
                      ],
                    ),
                  ),
                );
              });
        });
  }

  void blockUser(int id) {
    set_block(Constants.authorization, Constants.token, id, 0);
  }

  void messenger(int id) {
    print("messager user_id: ");
    print(id);
  }

  void cancelFriend(int id) {
    print("huy ket ban user_id: ");
    print(id);
  }
}
