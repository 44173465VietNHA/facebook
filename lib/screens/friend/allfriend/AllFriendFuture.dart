import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/screens/friend/allfriend/ListAllFriend.dart';
import 'package:flutter/material.dart';

class AllFriendFuture extends StatelessWidget {
  final Future<GetFriendResponse> friendFuture;
  AllFriendFuture({@required this.friendFuture});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<GetFriendResponse>(
        future: friendFuture,
        builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListAllFriend(snapshot.data.data.friends);
            } else if (snapshot.hasError) {
              return Text("Loi");
            } 
            // else if(!snapshot.hasData) {
            //   return Container(
            //   padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
            //   color: Colors.white,
            //   child: Text("Không có bạn bè nào")
            //   );
            // }
          // By default, show a loading spinner.
          return Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              color: Colors.white,
              child: CircularProgressIndicator()
          );
        },
      ),
    );
  }
}
