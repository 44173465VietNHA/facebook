import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:facebook/screens/friend/allfriend/AllFriendFuture.dart';
import 'package:facebook/screens/friend/allfriend/CountFriendFuture.dart';
import 'package:flutter/material.dart';

class AllFriendContainer extends StatelessWidget {
  final Future<GetFriendResponse> getFriendsResponse;
  AllFriendContainer({@required this.getFriendsResponse});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 2, 0, 0),
      color: Color.fromRGBO(201, 204, 209, 1),
      child: Container(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        color: Colors.white,
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CountFriendFuture(
                  friendFuture: getFriendsResponse,
                ),
                Expanded(
                    child: Container(
                  padding: EdgeInsets.only(top: 3),
                  child: Text(
                    "",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(220, 47, 74, 1),
                    ),
                  ),
                )),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                      padding: EdgeInsets.fromLTRB(0, 5, 10, 0),
                      child: Text(
                        "Sắp xếp",
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: 20,
                          // fontWeight: FontWeight.bold,
                        ),
                      )),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                AllFriendFuture(
                  friendFuture: getFriendsResponse,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
