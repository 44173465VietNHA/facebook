import 'package:facebook/api/payload/GetFriendResponse.dart';
import 'package:flutter/material.dart';

class CountFriendFuture extends StatelessWidget {
  final Future<GetFriendResponse> friendFuture;
  CountFriendFuture({@required this.friendFuture});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 0, 5),
      child: FutureBuilder<GetFriendResponse>(
        future: friendFuture,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Text(
              snapshot.data.data.total.toString() + " Bạn bè",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            );
          } else if (snapshot.hasError) {
            return Text("Loi");
          }
          // else if(!snapshot.hasData) {
          //   return Container(
          //   padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
          //   color: Colors.white,
          //   child: Text("Không có bạn bè nào")
          //   );
          // }
          // By default, show a loading spinner.
          return Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              color: Colors.white,);
        },
      ),
    );
  }
}
