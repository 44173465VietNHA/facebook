import 'package:facebook/api/payload/FriendsData.dart';
import 'package:facebook/screens/friend/allfriend/SingleAllFriend.dart';
import 'package:facebook/screens/friend/friendtab/SingleLoiMoi.dart';
import 'package:facebook/screens/friend/suggestfiend/SuggestSingle.dart';
import 'package:flutter/material.dart';

class ListSuggestFriend extends StatefulWidget {
  final List<FriendsData> data;
  ListSuggestFriend(@required this.data);
  @override
  _MyListState createState() => _MyListState(data);
}

class _MyListState extends State<ListSuggestFriend> {
  final List<FriendsData> data;
  _MyListState(@required this.data);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
            itemCount: data.length,
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 0),
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return SuggestSingle(data[index]);
            },
          );
  }
}
