
import 'package:facebook/api/comment/get_comment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:facebook/common/constants.dart' as Constants;
import 'package:flutter/rendering.dart';

class SingleComment extends StatefulWidget {
  GetCommentData data;
  SingleComment(@required this.data);

  @override
  _DescriptionTextWidgetState createState() =>
      new _DescriptionTextWidgetState(data);
}

class _DescriptionTextWidgetState extends State<SingleComment> {
  bool is_Like_True = false;
  GetCommentData data;
  _DescriptionTextWidgetState(@required this.data);
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size
        ;
    return Container(
    child: Row(
      children: [
        Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 10,  bottom: 7, right: 10),
              child: CircleAvatar(
                radius: 25,
                backgroundImage:  NetworkImage(Constants.FileURI + data.poster.avatar),
              ),
            ),
          ],
        ),

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: <Widget>[
                Container(

                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                   color: Color.fromRGBO(240, 242, 245, 1)
                  ),
                  padding: EdgeInsets.only(left: 5, right: 10),
                  width: size.width*0.75,

                  child: Container(
                    padding: EdgeInsets.all(7),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [

                    Text(data.poster.name,style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black),),
                    Container(height: 3,),
                    Text(data.comment, style: TextStyle(fontSize: 16),overflow: TextOverflow.fade,),

                  ],
                ),
                    ),

                ),
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 10, top: 5, bottom: 10),
                  child: Row(
                    children: <Widget>[
                      Container(
                        child: Text("${data.created}", style: TextStyle(color: Color.fromRGBO(118, 120, 123, 1), fontWeight: FontWeight.bold,fontSize: 15)),
                      ),
                      Container(
                        child: InkWell(
                        onTap: (){
                            setState(() {
                              is_Like_True = !is_Like_True;
                            });
                        },
                        child: Text("   Thích", style: TextStyle(color:(is_Like_True == true) ? Colors.blue : Color.fromRGBO(118, 120, 123, 1), fontWeight: FontWeight.bold,fontSize: 15)),
                      ),
                      ),
                      Container(
                      child: InkWell(
                      onTap: (){

                      },
                        child: Text("   Trả lời", style: TextStyle(color: Color.fromRGBO(118, 120, 123, 1), fontWeight: FontWeight.bold,fontSize: 15)),
                      ),
                      )



                    ]),
                  ),


              ],
            ),


          ],
        )
      ],
    ),
  );
  }
}
