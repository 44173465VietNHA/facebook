
import 'package:facebook/api/comment/get_comment.dart';
import 'package:facebook/screens/comment/SingleComment.dart';
import 'package:flutter/material.dart';

class ListSingleComment extends StatelessWidget {
  List<GetCommentData> data;
  ListSingleComment(this.data);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: data.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.only(top: 0),
      itemBuilder: (BuildContext context, int index) {
        return SingleComment(data[index]);
      },
    );
  }
}