import 'package:facebook/screens/example/toDoViewModel.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/toDoState.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ToDoInputScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("ToDoInput"),
          automaticallyImplyLeading: false,
        ),
        body: Column(children: <Widget>[
          StoreConnector<AppState, AlbumByUserIdViewModel>(
              distinct: true,
              converter: (store) => AlbumByUserIdViewModel.fromStore(store),
              builder: (context, viewModel) => TextField(
                    decoration: new InputDecoration(labelText: "Enter userId"),
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    maxLength: 2,
                    onSubmitted: (String value) {
                      return viewModel.dispatch(value);
                    },
                  )),
          StoreConnector<AppState, List<Album>>(
              distinct: true,
              converter: (store) => store.state.listAlbums,
              builder: (context, viewModel) {
                return Expanded( child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    padding: const EdgeInsets.all(10),
                    addAutomaticKeepAlives: true,
                    itemCount: (viewModel == null) ? 0 : viewModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 70,
                        child: Center(
                            child: Text(
                          viewModel[index].title,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                      );
                    }));
              })
        ]));
  }
}
