import 'package:facebook/screens/messenger/localComponents/messengerSettingList.dart';
import 'package:facebook/screens/messenger/localComponents/messengerTopBarOnlyTitle.dart';
import 'package:facebook/screens/messenger/messengerFakeData.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:facebook/stores/appState.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:facebook/common/constants.dart' as Constants;

class MessengerSettingScreen extends StatelessWidget {
  final String userAvatar;
  MessengerSettingScreen({@required this.userAvatar});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MessengerTopBarOnlyTitle(title: "Tôi"),
          Divider(color: Colors.black38, thickness: 0.6, height: 0),
          StoreConnector<AppState, UserInfoState>(
            distinct: true,
            converter: (store) => store.state.userInfo,
            builder: (context, viewModel) => MessengerSettingList(imageAvatar: viewModel.avatar, username: viewModel.username, id: Constants.user_id.toString()),
          )
        ],
      )
    );
  }
}