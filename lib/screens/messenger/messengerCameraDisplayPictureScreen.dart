import 'dart:developer';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class MessengerCameraDisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const MessengerCameraDisplayPictureScreen({Key key, @required this.imagePath}) : super(key: key);

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> _getLocalFile() async {
    final path = await _localPath;
    final name = DateTime.now().millisecondsSinceEpoch;
    return File('$path/$name');
  }

  // Future<File> writeImg(String path) async {
  //   final File file = await _getLocalFile();

  //   // Write the file.
  //   // return file.writeAsBytes(File(path))
  // }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          alignment: Alignment.center,
          child: Image.file(File(imagePath)),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: EdgeInsets.fromLTRB(10, 20, 0, 0),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                customBorder: CircleBorder(),
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.arrow_back, color: Colors.white, size: 30),
              )
            )
          )
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                customBorder: CircleBorder(),
                onTap: () => log("Tap save image"),
                child: Icon(Icons.save_alt, color: Colors.white),
              )
            )
          )
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: FloatingActionButton(
              backgroundColor: Colors.white,
              onPressed: () => log("Tap go to pick friend to send"),
              child: Icon(Icons.arrow_forward, color: Colors.black, size: 30),
              heroTag: null,
            )
          )
        )
      ]
    );
  }
}