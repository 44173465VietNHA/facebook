import 'dart:developer';
import 'package:flutter/material.dart';

class MessengerBoxUserBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      alignment: Alignment.center,
      child: Center(
        child: Column(
          children: [
            InkWell(
              customBorder: CircleBorder(),
              onTap: () => log("Tap show profile friend"),
              child: CircleAvatar(
                backgroundColor: Colors.grey[300],
                radius: 20,
                child: Icon(Icons.person, size: 30, color: Colors.black)
              ),
            ),
            SizedBox(height: 5),
            Text("Trang cá nhân", style: TextStyle(color: Colors.black, fontSize: 10)),
            SizedBox(height: 15),
            InkWell(
              onTap: () => log("Tap block friend") ,
              child: Container(
                padding: EdgeInsets.only(left: 30),
                height: 35,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Chặn", style: TextStyle(color: Colors.black, fontSize: 14))
                )
              )
            )
          ]
        )
      )
    );
  }
}