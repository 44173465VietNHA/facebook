import 'package:facebook/screens/messenger/localComponents/messengerNewMessageSlot.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MessengerNewMessageList extends StatelessWidget {
  final List<Map<String, String>> data;
  MessengerNewMessageList({@required this.data});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: data.length,
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 0),
        itemBuilder: (BuildContext context, int index) {
          return MessengerNewMessageSlot(dataFriend: data[index]);
        }
      )
    );
  }
}