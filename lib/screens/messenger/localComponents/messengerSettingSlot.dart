import 'dart:developer';
import 'package:flutter/material.dart';

class MessengerSettingSlot extends StatelessWidget {
  final Widget icon;
  final String title;
  final Color color;
  final Widget trailing;
  final Widget subtitle;
  final void Function() callback;
  MessengerSettingSlot({@required this.icon, @required this.title, @required this.color, this.trailing, this.callback, this.subtitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      alignment: Alignment.center,
      child: ListTile(
        onTap: () {
          log("Tap slot setting: " + title);
          if (callback != null)
            callback();
        },
        leading: Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          alignment: Alignment.center,
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: color
          ),
          child: ClipOval(
            child: icon
          )
        ),
        title: Text(title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
        trailing: trailing, // can be null
        subtitle: subtitle,
      )
    );
  }
}