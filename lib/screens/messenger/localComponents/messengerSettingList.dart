import 'dart:developer';
import 'package:facebook/globalComponents/toggleSwitch.dart';
import 'package:facebook/screens/messenger/localComponents/messengerSettingSlot.dart';
import 'package:facebook/screens/messenger/localComponents/messengerSettingUserInfo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class MessengerSettingList extends StatelessWidget {
  final String imageAvatar;
  final String username;
  final String id;
  MessengerSettingList({@required this.imageAvatar, @required this.username, @required this.id});
  @override
  Widget build(BuildContext context) {
    log("id" + id.toString());
    return Expanded(
      child: ListView(
        padding: EdgeInsets.only(top: 0),
        children: [
          MessengerSettingUserInfo(userAvatar: imageAvatar, username: username),
          MessengerSettingSlot(
            icon: Icon(Ionicons.ios_moon, color: Colors.white),
            title: "Chế độ tối",
            color: Colors.black,
            callback: () => log("Tap chuyển trạng thái sang chế độ tối"),
            trailing: ToggleSwitch(callback: (value) => log("Callback value is: " + value.toString()))
          ),
          MessengerSettingSlot(
            icon: Icon(MaterialCommunityIcons.chat_processing, color: Colors.white),
            title: "Tin nhắn đang chờ",
            color: Colors.blue[300],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(12, 10, 0, 2),
            child: Text("Trang cá nhân", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300))
          ),
          MessengerSettingSlot(
            icon: Icon(MaterialCommunityIcons.checkbox_multiple_blank_circle, color: Colors.white),
            title: "Trạng thái hoạt động",
            color: Colors.green,
            subtitle: Text("Bật", style: TextStyle(color: Colors.grey, fontSize: 12))
          ),
          MessengerSettingSlot(
            icon: Text('\u0040', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white)),
            title: "Tên người dùng",
            subtitle: RichText(
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              text: TextSpan(
                style: TextStyle(color: Colors.black),
                children: [
                  TextSpan(text: "m.me/", style: TextStyle(color: Colors.grey, fontSize: 12)),
                  TextSpan(text: this.id, style: TextStyle(color: Colors.black, fontSize: 12)),
                ]
              )
            ),            
            color: Colors.red
          ),
          Container(
            padding: EdgeInsets.fromLTRB(12, 10, 0, 2),
            child: Text("Tùy chọn", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300))
          ),
          MessengerSettingSlot(
            icon: Icon(MaterialCommunityIcons.shield, color: Colors.white, size: 20),
            title: "Quyền riêng tư",
            color: Colors.lightBlueAccent
          ),
        ],
      )
    );
  }
}