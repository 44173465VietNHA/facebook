import 'dart:developer';
import 'package:facebook/router/Router.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:facebook/actions/userInfoAction.dart';
import 'package:facebook/common/constants.dart';
import 'package:facebook/stores/userInfoState.dart';
import 'package:flutter/material.dart';

class MessengerChatTopBar extends StatelessWidget {
  final UserInfoState friend = chatFriendInfo;
  MessengerChatTopBar();
  @override
  Widget build(BuildContext context) {
    log("friend" + friend.id);
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 13, 4, 13),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              InkWell(
                onTap: () => Navigator.pop(context),
                customBorder: CircleBorder(),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 0, 8),
                  child: Icon(Icons.arrow_back, size: 30, color: Colors.blue)
                )
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: ClipOval(
                  child: CachedNetworkImage(
                    imageUrl: (friend == null) ? "https://i.pinimg.com/236x/f0/82/f9/f082f9421f5634fdc53bfa86b42b332f.jpg" : FileURI+friend.avatar,
                    errorWidget: (context, url, error) => CircularProgressIndicator(),
                    fit: BoxFit.cover,
                    width: 30,
                    height: 30,
                  )
                )
              ),
              Text((friend == null) ? "" : friend.username, textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold), overflow: TextOverflow.ellipsis),
            ]
          ),
          Padding(
            padding: EdgeInsets.all(3),
            child: InkWell(
              customBorder: CircleBorder(),
              onTap: () => Navigator.pushNamed(context, MESSENGER_BOX, arguments: {"avatar": friend.avatar, "username": friend.username}),
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                child: Icon(Icons.info, color: Colors.blue, size: 35)
              )
            )
          )
        ]
      )
    );
  }
}