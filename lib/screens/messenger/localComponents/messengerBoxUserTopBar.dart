import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class MessengerBoxUserTopBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 13, 10, 13),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 0),
            child: InkWell(
              onTap: () => Navigator.pop(context),
              customBorder: CircleBorder(),
              child: Icon(Icons.arrow_back, size: 30)
            ),
          ),
          Expanded(
            child: Container()
          ),
          InkWell(
            onTap: () => log("Tap option box user Info"),
            customBorder: CircleBorder(),
            child: Icon(MaterialCommunityIcons.dots_vertical)
          )
        ]
      )
    );
  }
}