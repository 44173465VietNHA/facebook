import 'package:facebook/screens/messenger/localComponents/messengerMainListSlot.dart';
import 'package:facebook/screens/messenger/localComponents/messengerWaitingTopBar.dart';
import 'package:flutter/material.dart';

class MessengerWaitingScreen extends StatelessWidget {
  final List<Map<String,String>> dataFriend;
  MessengerWaitingScreen({@required this.dataFriend});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MessengerWaitingTopBar(),
          MessengerMainListSlot(data: dataFriend)
        ],
      )
    );
  }
}