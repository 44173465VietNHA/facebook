import 'package:facebook/screens/messenger/localComponents/messengerBoxUserBody.dart';
import 'package:facebook/screens/messenger/localComponents/messengerBoxUserTopBar.dart';
import 'package:facebook/screens/messenger/localComponents/messengerSettingUserInfo.dart';
import 'package:facebook/screens/messenger/messengerFakeData.dart';
import 'package:flutter/material.dart';

class MessengerBoxUserScreen extends StatelessWidget {
  final String userAvatar;
  final String username;
  MessengerBoxUserScreen({@required this.userAvatar, @required this.username});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          MessengerBoxUserTopBar(),
          Divider(color: Colors.black38, thickness: 0.6, height: 0),
          MessengerSettingUserInfo(userAvatar: userAvatar, username: username),
          MessengerBoxUserBody()
        ],
      )
    );
  }
}