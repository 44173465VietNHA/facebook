import 'package:facebook/stores/deviceInfo.dart';

class SetDeviceInfoAction {
  final DeviceInfo deviceInfo;

  SetDeviceInfoAction(this.deviceInfo);
}

class SetUUIDAction {
  final String uuid;

  SetUUIDAction(this.uuid);
}
