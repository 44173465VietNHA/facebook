import 'package:facebook/stores/userInfoState.dart';
import 'package:meta/meta.dart';

UserInfoState chatFriendInfo;

class SetUserInfoAction {
  final UserInfoState userInfo;

  SetUserInfoAction({@required this.userInfo});
}

class SetFriendsAction {
  final List<UserInfoState> friends;
  SetFriendsAction({@required this.friends});
}