import 'package:collection/collection.dart';
import 'package:facebook/actions/notifcationAction.dart';
import 'package:facebook/stores/notificationState.dart';
import 'package:redux/redux.dart';

final listNotificationsReducer = combineReducers<List<NotificationSlotState>>({
  TypedReducer<List<NotificationSlotState>, SetListNotifications>(_setListNotificationsReducer),
});

List<NotificationSlotState> _setListNotificationsReducer(
    List<NotificationSlotState> list, SetListNotifications action) {
  return (list.length == action.listNotifcations.length && DeepCollectionEquality().equals(list, action.listNotifcations))
      ? list
      : action.listNotifcations;
}
