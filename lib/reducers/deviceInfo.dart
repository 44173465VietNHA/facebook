import 'package:facebook/actions/deviceInfoAction.dart';
import 'package:facebook/stores/deviceInfo.dart';
import 'package:redux/redux.dart';

final deviceInfoReducer = combineReducers<DeviceInfo>({
  TypedReducer<DeviceInfo, SetDeviceInfoAction>(_setDeviceInfoReducer),
  TypedReducer<DeviceInfo, SetUUIDAction>(_setUUIDReducer),
});

DeviceInfo _setDeviceInfoReducer(
    DeviceInfo deviceInfo, SetDeviceInfoAction action) {
  return (deviceInfo == action.deviceInfo) ? deviceInfo : action.deviceInfo;
}

DeviceInfo _setUUIDReducer(DeviceInfo deviceInfo, SetUUIDAction action) {
  return (deviceInfo.uuid == action.uuid) ? deviceInfo : deviceInfo.copyWith(uuid: action.uuid);
}